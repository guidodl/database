--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.2
-- Dumped by pg_dump version 9.6.1

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET escape_string_warning = off;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

--
-- Name: bonus; Type: TYPE; Schema: public; Owner: dungeonasdb
--

CREATE TYPE bonus AS (
	attacco integer,
	difesa integer,
	percezione integer,
	punti_ferita integer
);


ALTER TYPE bonus OWNER TO dungeonasdb;

--
-- Name: character_personaggio; Type: TYPE; Schema: public; Owner: dungeonasdb
--

CREATE TYPE character_personaggio AS (
	forza integer,
	intelligenza integer,
	costituzione integer,
	agilita integer
);


ALTER TYPE character_personaggio OWNER TO dungeonasdb;

--
-- Name: dup_result; Type: TYPE; Schema: public; Owner: dungeonasdb
--

CREATE TYPE dup_result AS (
	f1 integer,
	f2 text
);


ALTER TYPE dup_result OWNER TO dungeonasdb;

--
-- Name: nemiciconqtaassociata; Type: TYPE; Schema: public; Owner: dungeonasdb
--

CREATE TYPE nemiciconqtaassociata AS (
	nomenemico character varying(12),
	qta integer
);


ALTER TYPE nemiciconqtaassociata OWNER TO dungeonasdb;

--
-- Name: nemico_stanza_dungeon; Type: TYPE; Schema: public; Owner: dungeonasdb
--

CREATE TYPE nemico_stanza_dungeon AS (
	nemico integer,
	stanza integer,
	dungeon integer
);


ALTER TYPE nemico_stanza_dungeon OWNER TO dungeonasdb;

--
-- Name: oggetto_della_transazione; Type: TYPE; Schema: public; Owner: dungeonasdb
--

CREATE TYPE oggetto_della_transazione AS (
	personaggio integer,
	nome_oggetto character varying(25),
	valore_mo integer
);


ALTER TYPE oggetto_della_transazione OWNER TO dungeonasdb;

--
-- Name: persobjqta; Type: TYPE; Schema: public; Owner: dungeonasdb
--

CREATE TYPE persobjqta AS (
	pers integer,
	obj integer,
	qta integer
);


ALTER TYPE persobjqta OWNER TO dungeonasdb;

--
-- Name: perswithoggetto; Type: TYPE; Schema: public; Owner: dungeonasdb
--

CREATE TYPE perswithoggetto AS (
	pers integer,
	oggetto integer
);


ALTER TYPE perswithoggetto OWNER TO dungeonasdb;

--
-- Name: trans; Type: TYPE; Schema: public; Owner: dungeonasdb
--

CREATE TYPE trans AS (
	id integer,
	pers integer,
	obj integer,
	scambio boolean
);


ALTER TYPE trans OWNER TO dungeonasdb;

--
-- Name: value_for_game; Type: TYPE; Schema: public; Owner: dungeonasdb
--

CREATE TYPE value_for_game AS (
	attacco integer,
	difesa integer,
	percezione integer,
	punti_ferita integer
);


ALTER TYPE value_for_game OWNER TO dungeonasdb;

--
-- Name: accetta_fine_transazione(integer); Type: FUNCTION; Schema: public; Owner: dungeonasdb
--

CREATE FUNCTION accetta_fine_transazione(id_trans integer) RETURNS void
    LANGUAGE sql
    AS $$UPDATE TRANSAZIONE SET FINITA = TRUE WHERE ID = ID_TRANS;$$;


ALTER FUNCTION public.accetta_fine_transazione(id_trans integer) OWNER TO dungeonasdb;

--
-- Name: associa_nemico_stanza_dungeon(integer, integer, integer); Type: FUNCTION; Schema: public; Owner: dungeonasdb
--

CREATE FUNCTION associa_nemico_stanza_dungeon(dung integer, st integer, nem integer) RETURNS bit
    LANGUAGE plpgsql
    AS $$DECLARE
N_S_T NEMICO_STANZA_DUNGEON;
BEGIN
 SELECT * FROM nemici_in_stanza INTO N_S_T WHERE nemico = nem AND dungeon = dung AND stanza = st;
 IF N_S_T is NULL THEN
 INSERT INTO NEMICI_IN_STANZA VALUES(nem, st,dung);
 END IF;
 RETURN 1;
END;$$;


ALTER FUNCTION public.associa_nemico_stanza_dungeon(dung integer, st integer, nem integer) OWNER TO dungeonasdb;

--
-- Name: associa_pers_oggetto(integer, integer); Type: FUNCTION; Schema: public; Owner: dungeonasdb
--

CREATE FUNCTION associa_pers_oggetto(pers integer, obj integer) RETURNS void
    LANGUAGE plpgsql
    AS $$DECLARE 
 PERSWITHOBJ PERSWITHOGGETTO;
BEGIN
 SELECT personaggio, oggetto into PERSWITHOBJ FROM OGGETTI_POSSEDUTI WHERE PERSONAGGIO = PERS AND OGGETTO = obj;
 IF PERSWITHOBJ IS NULL THEN
   INSERT INTO OGGETTI_POSSEDUTI(PERSONAGGIO,OGGETTO,QUANTITA) VALUES (PERS, OBJ, 1);
 ELSE
   UPDATE OGGETTI_POSSEDUTI SET QUANTITA = QUANTITA + 1 WHERE PERSONAGGIO = PERS AND OGGETTO = OBJ;
 END IF;
END;$$;


ALTER FUNCTION public.associa_pers_oggetto(pers integer, obj integer) OWNER TO dungeonasdb;

--
-- Name: attacco_a_nemico_DA RIVEDERE(integer, integer, integer, integer); Type: FUNCTION; Schema: public; Owner: dungeonasdb
--

CREATE FUNCTION "attacco_a_nemico_DA RIVEDERE"(pers integer, dung integer, st integer, nem integer) RETURNS bit
    LANGUAGE plpgsql
    AS $$DECLARE
A INTEGER;
ATT INTEGER;
DIF INTEGER;
DADO INTEGER;
D INTEGER;
BEGIN
 SELECT ATTACCO INTO ATT FROM PERSONAGGIO WHERE ID = PERS;
 SELECT DIF INTO DIF FROM NEMICO WHERE ID = NEM;
 SELECT CEIL(RANDOM()*20) INTO DADO;
 SELECT OA.DANNO INTO D FROM OGGETTO_ATTACCO OA JOIN OGGETTO_UTILIZZATO_IN_STANZA OUIS ON OA.ID = OD.OGGETTO AND OUIS.PERSONAGGIO = PERS AND OUIS.DUNGEON = DUNG;
 IF (D IS NULL) THEN
  RETURN 0;
 END IF;
 A := ATT - DIF;
 A := A + DADO;
 IF (A > 12) THEN 
    UPDATE NEMICI_IN_STANZA SET PF_GIOCO = PF_GIOCO - D WHERE NEMICO = NEM AND DUNGEON = DUNG;
 ELSE
    RETURN 0; 
END IF;
 RETURN 1;
END;$$;


ALTER FUNCTION public."attacco_a_nemico_DA RIVEDERE"(pers integer, dung integer, st integer, nem integer) OWNER TO dungeonasdb;

--
-- Name: attacco_a_personaggio(integer, integer); Type: FUNCTION; Schema: public; Owner: dungeonasdb
--

CREATE FUNCTION attacco_a_personaggio(pers integer, dung integer) RETURNS bit
    LANGUAGE plpgsql
    AS $$DECLARE
IDS_NEMICO INTEGER[];
ST INTEGER;
N INTEGER;
D INTEGER;
BEGIN
SELECT STANZA INTO ST FROM POSIZIONE_PERSONAGGIO WHERE ATTUALMENTE = TRUE AND DUNGEON_GIOCATO = DUNG;
IDS_NEMICO := get_nemici_in_stanza(dung, st);
FOR N IN ARRAY_LOWER(IDS_NEMICO, 1) .. ARRAY_UPPER(IDS_NEMICO,1) LOOP
 SELECT DANNO INTO D WHERE ID = IDS_NEMICO[I];
 UPDATE DUNGEON_GIOCATO SET PF_GIOCO = PF_GIOCO - D WHERE PERSONAGGIO = PERS AND DUNGEON = DUNG;
END LOOP;
RETURN 1;
END;$$;


ALTER FUNCTION public.attacco_a_personaggio(pers integer, dung integer) OWNER TO dungeonasdb;

--
-- Name: calcola_valore_mo(integer); Type: FUNCTION; Schema: public; Owner: dungeonasdb
--

CREATE FUNCTION calcola_valore_mo(obj integer) RETURNS integer
    LANGUAGE plpgsql
    AS $$DECLARE
VAL INTEGER;
B BONUS;
MEDIA INTEGER;
SOMMAB INTEGER;
BEGIN
SELECT ATTACCO, DIFESA, PERCEZIONE, PUNTI_FERITA INTO B FROM OGGETTO WHERE ID = OBJ;
SOMMAB := 0;
SOMMAB := SOMMAB+ B.ATTACCO;
SOMMAB := SOMMAB+ B.DIFESA;
SOMMAB := SOMMAB + B.PERCEZIONE;
SOMMAB := SOMMAB + B.PUNTI_FERITA;
MEDIA := TRUNC(SOMMAB / 4);
IF (MEDIA < 0) THEN
 MEDIA := 0;
END IF;
VAL := MEDIA;
UPDATE OGGETTO SET VALORE_MO = VAL WHERE ID = OBJ;
RETURN VAL;
END;$$;


ALTER FUNCTION public.calcola_valore_mo(obj integer) OWNER TO dungeonasdb;

--
-- Name: check_morte(integer, integer); Type: FUNCTION; Schema: public; Owner: dungeonasdb
--

CREATE FUNCTION check_morte(dung integer, pers integer) RETURNS bit
    LANGUAGE plpgsql
    AS $$DECLARE
IDS_NEMICI_MORTI INTEGER[];
I INTEGER;
ST INTEGER;
PF_PERS INTEGER;
BEGIN

SELECT PF_GIOCO INTO PF_PERS FROM DUNGEON_GIOCATO WHERE DUNGEON = DUNG AND PERSONAGGIO = PERS;
IDS_NEMICI_MORTI := ARRAY(SELECT NEMICO FROM NEMICI_IN_STANZA WHERE DUNGEON = DUNG AND PF_GIOCO <= 0);

FOR I IN ARRAY_LOWER(IDS_NEMICI_MORTI, 1) .. ARRAY_UPPER(IDS_NEMICI_MORTI,1) LOOP
 SELECT STANZA INTO ST FROM NEMICI_IN_STANZA WHERE NEMICO = IDS_NEMICO[I] AND DUNGEON_GIOCATO = DUNG;
 DELETE FROM NEMICI_IN_STANZA WHERE NEMICO = IDS_NEMICI_MORTI[I];
 INSERT INTO NEMICI_SCONFITTI VALUES(IDS_NEMICI_MORTI[I], DUNG, ST);
END LOOP;

RETURN 0;
END;$$;


ALTER FUNCTION public.check_morte(dung integer, pers integer) OWNER TO dungeonasdb;

--
-- Name: crea_dungeon(character varying, integer); Type: FUNCTION; Schema: public; Owner: dungeonasdb
--

CREATE FUNCTION crea_dungeon(name character varying, pers integer) RETURNS bit
    LANGUAGE plpgsql
    AS $$DECLARE
STANZE INTEGER[];
I INTEGER;
K INTEGER;
J INTEGER;
N INTEGER;
COUNT_NEMICI INTEGER;
COUNT_OGGETTI INTEGER;
NEMICI INTEGER[];
ID_NEMICO INTEGER;
DADO INTEGER;
VISIBILE BOOLEAN;
ID_OGGETTO INTEGER;
TROVATO BOOLEAN;
NEMICI_INSERITI INTEGER;
BEGIN
 INSERT INTO DUNGEON(NOME) VALUES(NAME);
 STANZE := CREA_PERCORSO(currval(pg_get_serial_sequence('dungeon', 'id'))::INTEGER);
 NEMICI := ARRAY(SELECT UNNEST(selezione_nemici_da_inserire(pers, currval(pg_get_serial_sequence('dungeon', 'id'))::INTEGER, null)) ORDER BY RANDOM());
 COUNT_NEMICI := 1;
 COUNT_OGGETTI := 5;
 NEMICI_INSERITI := 0;
 I:= ARRAY_LOWER(NEMICI,1);
 FOR S IN ARRAY_LOWER(STANZE, 1) .. ARRAY_UPPER(STANZE,1) LOOP
  FOR K IN 1..COUNT_OGGETTI LOOP
    SELECT RANDOM() INTO DADO;
    IF (DADO > 0.70) THEN
      VISIBILE := FALSE;
    ELSE
      VISIBILE := TRUE;
    END IF;
    SELECT ID INTO ID_OGGETTO FROM OGGETTO OFFSET floor(random()*1000) LIMIT 1;
    INSERT INTO POSIZIONE_OGGETTO VALUES (ID_OGGETTO, VISIBILE, STANZE[S],currval(pg_get_serial_sequence('dungeon', 'id'))); 
  END LOOP;
    COUNT_OGGETTI := COUNT_OGGETTI + 5;
    FOR J IN 1..COUNT_NEMICI LOOP
      IF (NEMICI_INSERITI < ARRAY_UPPER(NEMICI, 1)) THEN
         ID_NEMICO := NEMICI[I];
         INSERT INTO NEMICI_IN_STANZA VALUES (ID_NEMICO, STANZE[S], currval(pg_get_serial_sequence('dungeon', 'id'))); 
         NEMICI_INSERITI := NEMICI_INSERITI + 1;
         I:= I+1;
      END IF;
    END LOOP;
    IF (S % 2 = 0) THEN
      COUNT_NEMICI := COUNT_NEMICI + 1;
    END IF;
  END LOOP;

RETURN 1;
END;$$;


ALTER FUNCTION public.crea_dungeon(name character varying, pers integer) OWNER TO dungeonasdb;

--
-- Name: crea_nemici(); Type: FUNCTION; Schema: public; Owner: dungeonasdb
--

CREATE FUNCTION crea_nemici() RETURNS void
    LANGUAGE plpgsql
    AS $$DECLARE
i INTEGER;
K INTEGER;
v character_personaggio;
danno INTEGER;
valPers value_for_game;
BEGIN
 FOR I IN 1..50 LOOP
 FOR K IN 3..18 LOOP
  Select ceil(Random()*K) INTO v.forza;
  select ceil(Random()*K) INTO v.agilita;
  select ceil(Random()*k) into v.intelligenza;
  select ceil(random()*k) into v.costituzione;
  valPers.attacco := ceil((v.forza + v.agilita)/2);
  valPers.difesa := ceil((v.costituzione + v.agilita)/2);
  valPers.punti_ferita := v.costituzione;
  SELECT CEIL(RANDOM()*9) INTO danno;
  INSERT INTO NEMICO(PUNTI_FERITA,DANNO,ATTACCO,DIFESA) VALUES(valPers.punti_ferita, danno, valPers.attacco, valPers.difesa);
 END LOOP;
 END LOOP;
END;$$;


ALTER FUNCTION public.crea_nemici() OWNER TO dungeonasdb;

--
-- Name: crea_oggetti(); Type: FUNCTION; Schema: public; Owner: dungeonasdb
--

CREATE FUNCTION crea_oggetti() RETURNS void
    LANGUAGE plpgsql
    AS $$DECLARE
IDO INTEGER;
I INTEGER;
J INTEGER;
D INTEGER;
NAME TEXT;
B BONUS;
IS_ATTACCO INTEGER;
IS_CONSUMABILE INTEGER;
BEGIN
 FOR I IN 0..1000 LOOP
  D := 0;
  SELECT CEIL(RANDOM() * 12) - 7 INTO B.ATTACCO;
  SELECT CEIL(RANDOM() * 12) - 7 INTO B.DIFESA;
  SELECT CEIL(RANDOM() * 12) - 7 INTO B.PERCEZIONE;
  SELECT CEIL(RANDOM() * 12) - 7 INTO B.PUNTI_FERITA;

  SELECT RANDOM() INTO IS_ATTACCO;
  IF (IS_ATTACCO > 0.49) THEN
   SELECT CEIL(RANDOM() * 15 + 1) INTO D;
   B.DIFESA := 0;
   select to_char(I, 'FM999999999999999999') || 'A' INTO NAME;
  ELSE
   SELECT RANDOM() INTO IS_CONSUMABILE;
   IF (IS_CONSUMABILE > 0.70) THEN
      IS_CONSUMABILE = 1;
     select to_char(I, 'FM999999999999999999') || 'C' INTO NAME;
    ELSE
     SELECT CEIL(RANDOM() * 5) INTO B.DIFESA;
     select to_char(I, 'FM999999999999999999') || 'D' INTO NAME;
    END IF;
  END IF;
  IDO := nextval(pg_get_serial_sequence('oggetto','id'));
  INSERT INTO OGGETTO (NOME, ATTACCO, DIFESA, PERCEZIONE , PUNTI_FERITA) VALUES (NAME, B.ATTACCO, B.DIFESA, B.PERCEZIONE, B.PUNTI_FERITA);
  IF (D > 0) THEN
   INSERT INTO OGGETTO_ATTACCO VALUES (currval(pg_get_serial_sequence('oggetto', 'id')), D);
  ELSIF  IS_CONSUMABILE = 1 THEN
   INSERT INTO OGGETTO_CONSUMABILE VALUES (currval(pg_get_serial_sequence('oggetto', 'id')));
  ELSE
   INSERT INTO OGGETTO_DIFESA VALUES (currval(pg_get_serial_sequence('oggetto', 'id')));
  END IF;
 END LOOP; 
END;$$;


ALTER FUNCTION public.crea_oggetti() OWNER TO dungeonasdb;

--
-- Name: crea_percorso(integer); Type: FUNCTION; Schema: public; Owner: dungeonasdb
--

CREATE FUNCTION crea_percorso(dung integer) RETURNS integer[]
    LANGUAGE plpgsql
    AS $$DECLARE
STANZE_SELEZIONATE INTEGER[];
STANZE_COLLEGATE BOOLEAN[];
NUM_STANZE_DUNGEON INTEGER;
CONT_SET_FLAG INTEGER;
ULTIMA_STANZA_COLLEGATA INTEGER;
NUM_COLLEGAMENTI_VISIBILI INTEGER;
NUM_COLLEGAMENTI_NASCOSTI INTEGER;
STANZA_INIZIALE INTEGER;
STANZA_FINALE INTEGER;
I INTEGER;
BEGIN
 SELECT CEIL(RANDOM() * GET_COUNT_STANZE()) INTO NUM_STANZE_DUNGEON;
 STANZE_SELEZIONATE := GET_STANZE_NON_RIPETUTE(NUM_STANZE_DUNGEON);
 STANZA_INIZIALE := STANZE_SELEZIONATE[0];
 STANZA_FINALE := STANZE_SELEZIONATE[NUM_STANZE_DUNGEON-1];

 PERFORM SET_STANZA_INIZIALE(DUNG, STANZA_INIZIALE);
 PERFORM SET_STANZA_FINALE(DUNG, STANZA_FINALE);
 CONT_SET_FLAG := NUM_STANZE_DUNGEON - 1;
 FOR I IN 0..CONT_SET_FLAG LOOP
   STANZE_COLLEGATE[STANZE_SELEZIONATE[I]] := FALSE;
 END LOOP;
 
 NUM_COLLEGAMENTI_VISIBILI := NUM_STANZE_DUNGEON -1;

 ULTIMA_STANZA_COLLEGATA := STANZA_INIZIALE;
 STANZE_COLLEGATE[STANZA_INIZIALE] := TRUE;
 FOR I IN 1..NUM_COLLEGAMENTI_VISIBILI LOOP
  INSERT INTO PASSAGGIO VALUES(ULTIMA_STANZA_COLLEGATA, STANZE_SELEZIONATE[I], DUNG);
  INSERT INTO PASSAGGIO VALUES(STANZE_SELEZIONATE[I], ULTIMA_STANZA_COLLEGATA, DUNG);
  ULTIMA_STANZA_COLLEGATA := STANZE_SELEZIONATE[I];
  STANZE_COLLEGATE[ULTIMA_STANZA_COLLEGATA] := TRUE;
 END LOOP;
 RETURN STANZE_SELEZIONATE;
END;
$$;


ALTER FUNCTION public.crea_percorso(dung integer) OWNER TO dungeonasdb;

--
-- Name: dai_nome_a_nemici(); Type: FUNCTION; Schema: public; Owner: dungeonasdb
--

CREATE FUNCTION dai_nome_a_nemici() RETURNS void
    LANGUAGE plpgsql
    AS $$DECLARE
NAME TEXT[];
I INTEGER;
NUMB INTEGER;
NUM_NEMICI INTEGER;
BEGIN
NAME[1] := 'ZOMBIE';
NAME[2] := 'CATTIVICK';
NAME[3] := 'CALLISTO';
NAME[4] := 'JOKER';
NAME[5] := 'DARKMAN';
NAME[6] := 'DRAGO';
NAME[7] := 'PICACHU';
NAME[8] := 'Torment';
NAME[9] := 'Dahaka';
SELECT COUNT(*) INTO NUM_NEMICI FROM NEMICO;
FOR I IN 1..NUM_NEMICI LOOP
 SELECT CEIL(RANDOM()*9) INTO NUMB;
 UPDATE NEMICO SET NOME = NAME[NUMB] WHERE ID = I;
END LOOP;
END;$$;


ALTER FUNCTION public.dai_nome_a_nemici() OWNER TO dungeonasdb;

--
-- Name: disattiva_oggetti_attivati_quando_dungeon_e_terminato(integer, integer); Type: FUNCTION; Schema: public; Owner: dungeonasdb
--

CREATE FUNCTION disattiva_oggetti_attivati_quando_dungeon_e_terminato(pers integer, dung integer) RETURNS void
    LANGUAGE plpgsql
    AS $$DECLARE
D INTEGER;
BEGIN
 SELECT DUNGEON INTO D FROM DUNGEON_SUPERATI WHERE DUNGEON = DUNG;
 IF D IS NOT NULL THEN
    UPDATE OGGETTI_POSSEDUTI SET ATTIVO = FALSE WHERE PERSONAGGIO = PERS;
 END IF;
END;$$;


ALTER FUNCTION public.disattiva_oggetti_attivati_quando_dungeon_e_terminato(pers integer, dung integer) OWNER TO dungeonasdb;

--
-- Name: generate_character_values(); Type: FUNCTION; Schema: public; Owner: dungeonasdb
--

CREATE FUNCTION generate_character_values() RETURNS integer[]
    LANGUAGE plpgsql
    AS $$

	DECLARE myArray	INTEGER[];

	BEGIN

		FOR i in 0..4

		LOOP

		myArray[i] := lancio_3d6();

		END LOOP;

		RETURN myArray;

	END

$$;


ALTER FUNCTION public.generate_character_values() OWNER TO dungeonasdb;

--
-- Name: FUNCTION generate_character_values(); Type: COMMENT; Schema: public; Owner: dungeonasdb
--

COMMENT ON FUNCTION generate_character_values() IS 'Restituisce i possibili valori da associare alle caratteristiche del personaggio(FOR, INT, AGI e COS).';


--
-- Name: generate_value_for_game_for_personaggio(integer, integer, integer); Type: FUNCTION; Schema: public; Owner: dungeonasdb
--

CREATE FUNCTION generate_value_for_game_for_personaggio(pers integer, dung integer, st integer) RETURNS value_for_game
    LANGUAGE plpgsql
    AS $$DECLARE
     valPers     value_for_game;
     valBonus    bonus;
     tempVal     character_personaggio;
BEGIN
     SELECT forza,intelligenza, costituzione,agilita INTO tempVal FROM personaggio WHERE id = pers;
     valBonus := get_bonus_ogg_(pers, dung, st);
     if (valBonus IS NOT NULL) THEN
         valPers.attacco := ceil((tempVal.forza + tempVal.agilita)/2) + valBonus.attacco;
         valPers.difesa := ceil((tempVal.costituzione + tempVal.agilita)/2) + valBonus.difesa;
         valPers.percezione := tempVal.intelligenza + valBonus.percezione;
         valPers.punti_ferita := tempVal.costituzione + valBonus.punti_ferita;
     else
         valPers.attacco := ceil((tempVal.forza + tempVal.agilita)/2);
         valPers.difesa := ceil((tempVal.costituzione + tempVal.agilita)/2);
         valPers.percezione := tempVal.intelligenza;
         valPers.punti_ferita := tempVal.costituzione;
     end if;
     return valPers;
      
END
$$;


ALTER FUNCTION public.generate_value_for_game_for_personaggio(pers integer, dung integer, st integer) OWNER TO dungeonasdb;

--
-- Name: get_bonus_ogg_(integer, integer, integer); Type: FUNCTION; Schema: public; Owner: dungeonasdb
--

CREATE FUNCTION get_bonus_ogg_(pers integer, dung integer, st integer) RETURNS bonus
    LANGUAGE plpgsql
    AS $$DECLARE
        i                   INTEGER;
        k                   INTEGER;
        objAttacco          INTEGER;
	objTempAttacco      bonus;		
	objDifesa	    INTEGER;
	objTempDifesa	    bonus;
	AttaccoPresente	    BOOLEAN:= FALSE;
	DifesaPresente 	    BOOLEAN:= FALSE;
	bonusOggetti	    bonus;
       BonusDef          bonus;
        objConsum           bonus;
    BEGIN
        bonusOggetti.attacco:= 0;
        bonusOggetti.difesa := 0;
        bonusOggetti.percezione := 0;
        bonusOggetti.punti_ferita := 0;
		if (dung IS NULL OR st IS NULL) THEN
			bonusDef.attacco := bonusOggetti.attacco;
                        bonusDef.difesa := bonusOggetti.difesa;
                        bonusDef.percezione := bonusOggetti.percezione;
                        bonusDef.punti_ferita := bonusOggetti.punti_ferita;
                        RETURN bonusDef;
                ELSE
			objAttacco := get_id_oggetti_attacco_attivato (pers, dung);
			IF (objAttacco is NOT NULL) THEN
				AttaccoPresente := TRUE;
SELECT attacco, difesa, percezione, punti_ferita INTO objTempAttacco FROM oggetto JOIN oggetto_attacco ON oggetto.id = objAttacco;
			END IF;
			objDifesa := get_id_oggetti_difesa_attivati (pers, dung);
			IF (objDifesa IS NOT NULL) THEN
		            DifesaPresente := TRUE;
			   SELECT attacco, difesa, percezione, punti_ferita INTO objTempDifesa FROM oggetto JOIN oggetto_consumabile_difesa ON oggetto.id = objDifesa;
			END IF;
			IF (DifesaPresente = TRUE AND AttaccoPresente = TRUE) THEN
			     bonusOggetti.attacco := objTempAttacco.attacco + objTempDifesa.attacco;
                             bonusOggetti.difesa := objTempAttacco.difesa + objTempDifesa.difesa; 
                             bonusOggetti.percezione := objTempAttacco.percezione + objTempDifesa.percezione; 
                             bonusOggetti.punti_ferita := objTempAttacco.punti_ferita + objTempDifesa.punti_ferita;                                
                        ELSIF(DifesaPresente = TRUE) then
				bonusOggetti := objTempDifesa;
			ELSIF(AttaccoPresente = TRUE) THEN
				bonusOggetti := objTempAttacco;
                        END IF;
                            objConsum := get_bonus_ogg_consum(pers, st, dung);
			IF(objConsum IS NOT NULL) THEN
			        			          bonusOggetti.attacco := bonusOggetti.attacco + objConsum.attacco;
                             bonusOggetti.difesa := bonusOggetti.difesa + objConsum.difesa; 
                             bonusOggetti.percezione := bonusOggetti.percezione + objConsum.percezione; 
                             bonusOggetti.punti_ferita := bonusOggetti.punti_ferita + objConsum.punti_ferita;            			                               END IF;
	   END IF;
           bonusDef.attacco := bonusOggetti.attacco;
           bonusDef.difesa := bonusOggetti.difesa;
           bonusDef.percezione := bonusOggetti.percezione;
           bonusDef.punti_ferita := bonusOggetti.punti_ferita;
                        RETURN bonusDef;
    END
$$;


ALTER FUNCTION public.get_bonus_ogg_(pers integer, dung integer, st integer) OWNER TO dungeonasdb;

--
-- Name: get_bonus_ogg_consum(integer, integer, integer); Type: FUNCTION; Schema: public; Owner: dungeonasdb
--

CREATE FUNCTION get_bonus_ogg_consum(pers integer, st integer, dung integer) RETURNS bonus
    LANGUAGE plpgsql
    AS $$DECLARE

        bon bonus;
        bonusDef bonus;
    BEGIN
		bonusDef.attacco := 0;
                        bonusDef.difesa := 0;
                        bonusDef.percezione := 0;
                        bonusDef.punti_ferita := 0; 
        FOR bon IN SELECT O.attacco, O.difesa, O.percezione, O.punti_ferita FROM oggetto_consumabile_difesa AS OCD JOIN oggetto_utilizzato_in_stanza as OU ON OCD.id = OU.OGGETTO JOIN oggetti_posseduti OP ON OU.oggetto = OP.oggetto JOIN oggetto O ON O.id = OP.oggetto

				 WHERE OU.personaggio = pers AND OU.stanza = st AND OP.personaggio = pers

				 AND OCD.consumabile = TRUE

        LOOP
 	bonusDef.attacco := bonusDef.attacco + bon.attacco;
     bonusDef.difesa := bonusDef.difesa + bon.difesa;                  bonusDef.percezione := bonusDef.percezione +bon.percezione;
                        bonusDef.punti_ferita := bonusDef.punti_ferita + bon.punti_ferita;

        END LOOP;

        RETURN bonusDef;

    END
$$;


ALTER FUNCTION public.get_bonus_ogg_consum(pers integer, st integer, dung integer) OWNER TO dungeonasdb;

--
-- Name: get_count_stanze(); Type: FUNCTION; Schema: public; Owner: dungeonasdb
--

CREATE FUNCTION get_count_stanze() RETURNS integer
    LANGUAGE plpgsql
    AS $$DECLARE
TOT INTEGER;
BEGIN
SELECT COUNT(*) INTO TOT FROM stanza;
RETURN TOT;
END;$$;


ALTER FUNCTION public.get_count_stanze() OWNER TO dungeonasdb;

--
-- Name: get_id_oggetti_attacco_attivato(integer, integer); Type: FUNCTION; Schema: public; Owner: dungeonasdb
--

CREATE FUNCTION get_id_oggetti_attacco_attivato(pers integer, dung integer) RETURNS integer
    LANGUAGE sql
    AS $$SELECT OA.id FROM oggetto_attacco AS OA JOIN oggetti_posseduti OP ON OA.id = OP.oggetto
				 WHERE OP.personaggio = pers AND OP.personaggio = pers AND OP.attivo = TRUE;$$;


ALTER FUNCTION public.get_id_oggetti_attacco_attivato(pers integer, dung integer) OWNER TO dungeonasdb;

--
-- Name: FUNCTION get_id_oggetti_attacco_attivato(pers integer, dung integer); Type: COMMENT; Schema: public; Owner: dungeonasdb
--

COMMENT ON FUNCTION get_id_oggetti_attacco_attivato(pers integer, dung integer) IS 'Restituisce l''id dell''oggetto di attacco attivato';


--
-- Name: get_id_oggetti_difesa_attivati(integer, integer); Type: FUNCTION; Schema: public; Owner: dungeonasdb
--

CREATE FUNCTION get_id_oggetti_difesa_attivati(pers integer, dung integer) RETURNS integer
    LANGUAGE sql
    AS $$    SELECT OU.oggetto FROM oggetto_consumabile_difesa AS OCD JOIN oggetto_utilizzato_in_stanza as OU ON OCD.id = OU.OGGETTO JOIN oggetti_posseduti OP ON OU.oggetto = OP.oggetto
				 WHERE OU.personaggio = pers AND OU.dungeon = dung AND OP.personaggio = pers
				 AND OCD.consumabile = FALSE AND OP.attivo = TRUE;$$;


ALTER FUNCTION public.get_id_oggetti_difesa_attivati(pers integer, dung integer) OWNER TO dungeonasdb;

--
-- Name: get_nemici_in_stanza(integer, integer); Type: FUNCTION; Schema: public; Owner: dungeonasdb
--

CREATE FUNCTION get_nemici_in_stanza(dung integer, st integer) RETURNS integer[]
    LANGUAGE plpgsql
    AS $$DECLARE
IDS_NEMICO INTEGER[];
BEGIN
IDS_NEMICO := ARRAY(SELECT NEMICO FROM NEMICI_IN_STANZA WHERE DUNGEON = DUNG AND STANZA = ST AND PF_GIOCO > 0
                    EXCEPT
                    SELECT NEMICO FROM NEMICI_SCONFITTI WHERE DUNGEON_GIOCATO = DUNG AND STANZA = ST);
RETURN IDS_NEMICO;
END;$$;


ALTER FUNCTION public.get_nemici_in_stanza(dung integer, st integer) OWNER TO dungeonasdb;

--
-- Name: get_stanza(); Type: FUNCTION; Schema: public; Owner: dungeonasdb
--

CREATE FUNCTION get_stanza() RETURNS integer
    LANGUAGE plpgsql
    AS $$DECLARE
ID INTEGER;
NUM_STANZE INTEGER;
BEGIN
 NUM_STANZE := get_count_stanze();
 select ceil(random() * NUM_STANZE) INTO ID;
RETURN ID;
END;
$$;


ALTER FUNCTION public.get_stanza() OWNER TO dungeonasdb;

--
-- Name: get_stanze_adiacenti(integer, integer); Type: FUNCTION; Schema: public; Owner: dungeonasdb
--

CREATE FUNCTION get_stanze_adiacenti(dung integer, st integer) RETURNS integer[]
    LANGUAGE plpgsql
    AS $$DECLARE
STANZE_ADIACENTI INTEGER[];
BEGIN
STANZE_ADIACENTI := ARRAY(
SELECT DISTINCT STANZA_ARRIVO FROM PASSAGGIO WHERE DUNGEON = DUNG AND STANZA_PROVENIENZA = ST AND SEGRETO = FALSE);
RETURN STANZE_ADIACENTI;
END;$$;


ALTER FUNCTION public.get_stanze_adiacenti(dung integer, st integer) OWNER TO dungeonasdb;

--
-- Name: get_stanze_non_ripetute(integer); Type: FUNCTION; Schema: public; Owner: dungeonasdb
--

CREATE FUNCTION get_stanze_non_ripetute(num_stanze_da_selezionare integer) RETURNS integer[]
    LANGUAGE plpgsql
    AS $$DECLARE
STANZE INTEGER[];
SELEZIONATE BOOLEAN[];
CONT INTEGER;
BEGIN
 CONT := 0;
 FOR i in 1..get_count_stanze() LOOP
 SELEZIONATE[i] := false;
 END LOOP;
 WHILE CONT < num_stanze_da_selezionare LOOP
   STANZE[CONT] := get_stanza();
   IF SELEZIONATE[STANZE[CONT]] = FALSE THEN
     SELEZIONATE[STANZE[CONT]] = TRUE;
     CONT := CONT + 1;
   END IF;
 END LOOP;
RETURN STANZE;
END;$$;


ALTER FUNCTION public.get_stanze_non_ripetute(num_stanze_da_selezionare integer) OWNER TO dungeonasdb;

--
-- Name: inserisci_proposta_transazione(integer, integer, boolean); Type: FUNCTION; Schema: public; Owner: dungeonasdb
--

CREATE FUNCTION inserisci_proposta_transazione(pers integer, obj integer, is_scambio boolean) RETURNS void
    LANGUAGE sql
    AS $$INSERT INTO TRANSAZIONE (ID_CEDENTE, OGGETTO_CEDENTE,DATA_TRANSAZIONE,SCAMBIO) VALUES (pers, obj, CURRENT_TIMESTAMP, is_scambio);$$;


ALTER FUNCTION public.inserisci_proposta_transazione(pers integer, obj integer, is_scambio boolean) OWNER TO dungeonasdb;

--
-- Name: lancio_3d6(); Type: FUNCTION; Schema: public; Owner: dungeonasdb
--

CREATE FUNCTION lancio_3d6() RETURNS integer
    LANGUAGE plpgsql
    AS $$

DECLARE

  finalValue integer := 0;

BEGIN

	FOR i in 1..3

	LOOP

		finalValue := finalValue + round(random()*(6-1)+1);

	END LOOP;

	RETURN finalValue;

END

$$;


ALTER FUNCTION public.lancio_3d6() OWNER TO dungeonasdb;

--
-- Name: FUNCTION lancio_3d6(); Type: COMMENT; Schema: public; Owner: dungeonasdb
--

COMMENT ON FUNCTION lancio_3d6() IS 'Restituisce la somma dei tre lanci di un dado.';


--
-- Name: nemici_piu_deboli(integer, integer, integer); Type: FUNCTION; Schema: public; Owner: dungeonasdb
--

CREATE FUNCTION nemici_piu_deboli(pers integer, dung integer, st integer) RETURNS integer[]
    LANGUAGE plpgsql
    AS $$DECLARE
NEM INTEGER[];
P PERSONAGGIO;
VAL_PERS VALUE_FOR_GAME;
MAX_ATT INTEGER;
MAX_DIF INTEGER;
MIN_ATT INTEGER;
MIN_DIF INTEGER;
NEM_SELEZIONATO BOOLEAN[];
NEM_LVL_MIN INTEGER[];
NEM_TEMP INTEGER[];
NEM_LVL_MAX INTEGER[];
NEM_LVL_RANDOM INTEGER[];
I INTEGER;
BEGIN
SELECT * INTO P FROM PERSONAGGIO WHERE ID = PERS;
VAL_PERS = generate_value_for_game_for_personaggio(PERS, DUNG, ST);
NEM := selezione_nemici_da_inserire(PERS, DUNG, ST);

FOR I IN 1..array_ndims(NEM) LOOP
 NEM_SELEZIONATO[NEM[I]] := FALSE;
END LOOP;
SELECT MIN(DIFESA) INTO MIN_DIF FROM NEMICO WHERE ID IN (SELECT(UNNEST(NEM)));
SELECT MIN(ATTACCO) INTO MIN_ATT FROM NEMICO WHERE ID IN (SELECT(UNNEST(NEM)));
SELECT MAX(DIFESA) INTO MAX_DIF FROM NEMICO WHERE ID IN (SELECT(UNNEST(NEM)));
SELECT MAX(ATTACCO) INTO MAX_ATT FROM NEMICO WHERE ID IN (SELECT(UNNEST(NEM)));
NEM_LVL_MIN := ARRAY(SELECT ID FROM NEMICO WHERE ATTACCO = MIN_ATT AND DIFESA < MAX_DIF);
FOR I IN 1..array_ndims(NEM_LVL_MIN) LOOP
 NEM_SELEZIONATO[NEM_LVL_MIN[I]] := TRUE;
END LOOP;
NEM_TEMP := ARRAY(SELECT ID FROM NEMICO WHERE DIFESA = MIN_DIF AND ATTACCO < MAX_ATT);

FOR I IN 1..array_ndims(NEM_TEMP) LOOP
 IF NEM_SELEZIONATO[NEM_TEMP[I]] = FALSE THEN
  NEM_SELEZIONATO[NEM_TEMP[I]] := TRUE;
  NEM_LVL_MIN := ARRAY_APPEND(NEM_LVL_MIN, NEM_TEMP[I]);
 END IF;
END LOOP;

RETURN NEM_LVL_MIN;
END;$$;


ALTER FUNCTION public.nemici_piu_deboli(pers integer, dung integer, st integer) OWNER TO dungeonasdb;

--
-- Name: per_ogni_ogg_valore_mo(); Type: FUNCTION; Schema: public; Owner: dungeonasdb
--

CREATE FUNCTION per_ogni_ogg_valore_mo() RETURNS void
    LANGUAGE plpgsql
    AS $$DECLARE 
K INTEGER;
val INTEGER;
BEGIN
 FOR K IN SELECT ID FROM OGGETTO LOOP
    val := calcola_valore_mo(k);
 END LOOP;

END;$$;


ALTER FUNCTION public.per_ogni_ogg_valore_mo() OWNER TO dungeonasdb;

--
-- Name: proponi_termine_transazione(integer, integer, integer); Type: FUNCTION; Schema: public; Owner: dungeonasdb
--

CREATE FUNCTION proponi_termine_transazione(id_trans integer, pers integer, obj integer) RETURNS void
    LANGUAGE sql
    AS $$UPDATE TRANSAZIONE SET ID_ACQUIRENTE = PERS, OGGETTO_ACQUIRENTE = OBJ WHERE ID = ID_TRANS;$$;


ALTER FUNCTION public.proponi_termine_transazione(id_trans integer, pers integer, obj integer) OWNER TO dungeonasdb;

--
-- Name: res_5_3d6_2(anyarray); Type: FUNCTION; Schema: public; Owner: dungeonasdb
--

CREATE FUNCTION res_5_3d6_2(f1 anyarray, OUT f2 anyarray) RETURNS anyarray
    LANGUAGE sql
    AS $_$select $1$_$;


ALTER FUNCTION public.res_5_3d6_2(f1 anyarray, OUT f2 anyarray) OWNER TO dungeonasdb;

--
-- Name: selezione_nemici_da_inserire(integer, integer, integer); Type: FUNCTION; Schema: public; Owner: dungeonasdb
--

CREATE FUNCTION selezione_nemici_da_inserire(pers integer, dung integer, st integer) RETURNS integer[]
    LANGUAGE plpgsql
    AS $$DECLARE
NEM INTEGER[];
P PERSONAGGIO;
VAL_PERS VALUE_FOR_GAME;
BEGIN
SELECT * INTO P FROM PERSONAGGIO WHERE ID = PERS;
VAL_PERS = generate_value_for_game_for_personaggio(PERS, DUNG, ST);
NEM := ARRAY(SELECT id FROM NEMICO AS N WHERE N.ATTACCO < VAL_PERS.ATTACCO AND N.DIFESA < VAL_PERS.DIFESA AND DANNO < VAL_PERS.PUNTI_FERITA);
RETURN NEM;
END;$$;


ALTER FUNCTION public.selezione_nemici_da_inserire(pers integer, dung integer, st integer) OWNER TO dungeonasdb;

--
-- Name: set_stanza_finale(integer, integer); Type: FUNCTION; Schema: public; Owner: dungeonasdb
--

CREATE FUNCTION set_stanza_finale(dung integer, st integer) RETURNS void
    LANGUAGE plpgsql
    AS $$DECLARE
BEGIN
INSERT INTO PASSAGGIO VALUES(st, NULL, dung);
END;$$;


ALTER FUNCTION public.set_stanza_finale(dung integer, st integer) OWNER TO dungeonasdb;

--
-- Name: set_stanza_iniziale(integer, integer); Type: FUNCTION; Schema: public; Owner: dungeonasdb
--

CREATE FUNCTION set_stanza_iniziale(dung integer, st integer) RETURNS void
    LANGUAGE plpgsql
    AS $$BEGIN
INSERT INTO PASSAGGIO VALUES(NULL, st, dung);
END;$$;


ALTER FUNCTION public.set_stanza_iniziale(dung integer, st integer) OWNER TO dungeonasdb;

--
-- Name: update_posizione_personaggio(integer, integer, integer, integer); Type: FUNCTION; Schema: public; Owner: dungeonasdb
--

CREATE FUNCTION update_posizione_personaggio(pers integer, dung integer, stanza_provenienza integer, stanza_arrivo integer) RETURNS void
    LANGUAGE plpgsql
    AS $$BEGIN
IF STANZA_PROVENIENZA IS NOT NULL THEN
  UPDATE SPOSTAMENTI_PERSONAGGIO SET ATTUALMENTE = FALSE WHERE PERSONAGGIO = PERS AND DUNGEON = DUNG AND STANZA =        STANZA_PROVENIENZA;
END IF;
INSERT INTO SPOSTAMENTI_PERSONAGGIO(PERSONAGGIO,STANZA,DUNGEON,ATTUALMENTE) VALUES(PERS,STANZA_ARRIVO,DUNG,TRUE);
END;$$;


ALTER FUNCTION public.update_posizione_personaggio(pers integer, dung integer, stanza_provenienza integer, stanza_arrivo integer) OWNER TO dungeonasdb;

--
-- Name: update_value_for_game(integer, character_personaggio); Type: FUNCTION; Schema: public; Owner: dungeonasdb
--

CREATE FUNCTION update_value_for_game(pers integer, val character_personaggio) RETURNS void
    LANGUAGE plpgsql
    AS $$BEGIN
UPDATE PERSONAGGIO SET ATTACCO = VAL.ATTACCO, DIFESA = VAL.DIFESA, PERCEZIONE = VAL.PERCEZIONE, PUNTI_FERITA = VAL.PUNTI_FERITA WHERE ID = PERS;
END;$$;


ALTER FUNCTION public.update_value_for_game(pers integer, val character_personaggio) OWNER TO dungeonasdb;

--
-- Name: visualizza_oggetti_posseduti(integer); Type: FUNCTION; Schema: public; Owner: dungeonasdb
--

CREATE FUNCTION visualizza_oggetti_posseduti(pers integer) RETURNS persobjqta[]
    LANGUAGE plpgsql
    AS $$DECLARE
POQ PERSOBJQTA[];
BEGIN
POQ := ARRAY(SELECT PERSONAGGIO, OGGETTO, QUANTITA FROM OGGETTI_POSSEDUTI WHERE PERSONAGGIO = PERSO);
RETURN POQ;
END;$$;


ALTER FUNCTION public.visualizza_oggetti_posseduti(pers integer) OWNER TO dungeonasdb;

--
-- Name: visualizza_transazioni_aperte_da_altri_personaggi(integer); Type: FUNCTION; Schema: public; Owner: dungeonasdb
--

CREATE FUNCTION visualizza_transazioni_aperte_da_altri_personaggi(pers integer) RETURNS trans[]
    LANGUAGE plpgsql
    AS $$DECLARE
POT TRANS[];
BEGIN
POT := ARRAY(SELECT ID, ID_CEDENTE, OGGETTO_CEDENTE, SCAMBIO FROM TRANSAZIONE WHERE ID_CEDENTE <> PERS AND FINITA <> TRUE);
RETURN POT;
END;$$;


ALTER FUNCTION public.visualizza_transazioni_aperte_da_altri_personaggi(pers integer) OWNER TO dungeonasdb;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: transazione; Type: TABLE; Schema: public; Owner: dungeonasdb
--

CREATE TABLE transazione (
    id_cedente integer NOT NULL,
    id_acquirente integer,
    oggetto_cedente integer NOT NULL,
    oggetto_acquirente integer,
    data_transazione timestamp without time zone NOT NULL,
    finita boolean DEFAULT false NOT NULL,
    id integer NOT NULL,
    scambio boolean NOT NULL
);


ALTER TABLE transazione OWNER TO dungeonasdb;

--
-- Name: visualizza_transazioni_da_accettare(integer); Type: FUNCTION; Schema: public; Owner: dungeonasdb
--

CREATE FUNCTION visualizza_transazioni_da_accettare(pers integer) RETURNS transazione[]
    LANGUAGE plpgsql
    AS $$DECLARE
T TRANSAZIONE[];
BEGIN
T:= ARRAY(SELECT * FROM TRANSAZIONE WHERE ID_CEDENTE = PERS AND ID_ACQUIRENTE IS NOT NULL AND OGGETTO_ACQUIRENTE IS NOT NULL AND TERMINATA = FALSE);
RETURN T;
END;$$;


ALTER FUNCTION public.visualizza_transazioni_da_accettare(pers integer) OWNER TO dungeonasdb;

--
-- Name: attacco; Type: TABLE; Schema: public; Owner: dungeonasdb
--

CREATE TABLE attacco (
    id integer NOT NULL,
    personaggio integer,
    nemico integer,
    danno integer,
    dungeon integer,
    stanza integer
);


ALTER TABLE attacco OWNER TO dungeonasdb;

--
-- Name: attacco_id_seq; Type: SEQUENCE; Schema: public; Owner: dungeonasdb
--

CREATE SEQUENCE attacco_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE attacco_id_seq OWNER TO dungeonasdb;

--
-- Name: attacco_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dungeonasdb
--

ALTER SEQUENCE attacco_id_seq OWNED BY attacco.id;


--
-- Name: dungeon; Type: TABLE; Schema: public; Owner: dungeonasdb
--

CREATE TABLE dungeon (
    id integer NOT NULL,
    nome character varying(50) NOT NULL,
    personaggio integer,
    pf_gioco integer NOT NULL
);


ALTER TABLE dungeon OWNER TO dungeonasdb;

--
-- Name: dungeon_giocato; Type: TABLE; Schema: public; Owner: dungeonasdb
--

CREATE TABLE dungeon_giocato (
    dungeon integer NOT NULL,
    personaggio integer NOT NULL,
    id integer NOT NULL,
    terminato integer NOT NULL
);


ALTER TABLE dungeon_giocato OWNER TO dungeonasdb;

--
-- Name: dungeon_giocati_id_seq; Type: SEQUENCE; Schema: public; Owner: dungeonasdb
--

CREATE SEQUENCE dungeon_giocati_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE dungeon_giocati_id_seq OWNER TO dungeonasdb;

--
-- Name: dungeon_giocati_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dungeonasdb
--

ALTER SEQUENCE dungeon_giocati_id_seq OWNED BY dungeon_giocato.id;


--
-- Name: dungeon_giocati_terminato_seq; Type: SEQUENCE; Schema: public; Owner: dungeonasdb
--

CREATE SEQUENCE dungeon_giocati_terminato_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE dungeon_giocati_terminato_seq OWNER TO dungeonasdb;

--
-- Name: dungeon_giocati_terminato_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dungeonasdb
--

ALTER SEQUENCE dungeon_giocati_terminato_seq OWNED BY dungeon_giocato.terminato;


--
-- Name: dungeon_id_seq; Type: SEQUENCE; Schema: public; Owner: dungeonasdb
--

CREATE SEQUENCE dungeon_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE dungeon_id_seq OWNER TO dungeonasdb;

--
-- Name: dungeon_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dungeonasdb
--

ALTER SEQUENCE dungeon_id_seq OWNED BY dungeon.id;


--
-- Name: dungeon_pf_gioco_seq; Type: SEQUENCE; Schema: public; Owner: dungeonasdb
--

CREATE SEQUENCE dungeon_pf_gioco_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE dungeon_pf_gioco_seq OWNER TO dungeonasdb;

--
-- Name: dungeon_pf_gioco_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dungeonasdb
--

ALTER SEQUENCE dungeon_pf_gioco_seq OWNED BY dungeon.pf_gioco;


--
-- Name: nemici_in_stanza; Type: TABLE; Schema: public; Owner: dungeonasdb
--

CREATE TABLE nemici_in_stanza (
    nemico integer NOT NULL,
    stanza integer NOT NULL,
    dungeon integer NOT NULL,
    pf_gioco integer
);


ALTER TABLE nemici_in_stanza OWNER TO dungeonasdb;

--
-- Name: nemici_sconfitti; Type: TABLE; Schema: public; Owner: dungeonasdb
--

CREATE TABLE nemici_sconfitti (
    id integer NOT NULL,
    nemico integer NOT NULL,
    dungeon_giocato integer NOT NULL,
    stanza integer
);


ALTER TABLE nemici_sconfitti OWNER TO dungeonasdb;

--
-- Name: nemici_sconfitti_id_seq; Type: SEQUENCE; Schema: public; Owner: dungeonasdb
--

CREATE SEQUENCE nemici_sconfitti_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE nemici_sconfitti_id_seq OWNER TO dungeonasdb;

--
-- Name: nemici_sconfitti_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dungeonasdb
--

ALTER SEQUENCE nemici_sconfitti_id_seq OWNED BY nemici_sconfitti.id;


--
-- Name: nemico; Type: TABLE; Schema: public; Owner: dungeonasdb
--

CREATE TABLE nemico (
    id integer NOT NULL,
    nome character varying(12) NOT NULL,
    punti_ferita integer NOT NULL,
    danno integer NOT NULL,
    attacco integer NOT NULL,
    difesa integer NOT NULL,
    CONSTRAINT nemico_attacco_check CHECK ((attacco > 0)),
    CONSTRAINT nemico_danno_check CHECK ((danno > 0)),
    CONSTRAINT nemico_difesa_check CHECK ((difesa > 0)),
    CONSTRAINT nemico_punti_ferita_check CHECK ((punti_ferita > 0))
);


ALTER TABLE nemico OWNER TO dungeonasdb;

--
-- Name: nemico_id_seq; Type: SEQUENCE; Schema: public; Owner: dungeonasdb
--

CREATE SEQUENCE nemico_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE nemico_id_seq OWNER TO dungeonasdb;

--
-- Name: nemico_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dungeonasdb
--

ALTER SEQUENCE nemico_id_seq OWNED BY nemico.id;


--
-- Name: oggetti_posseduti; Type: TABLE; Schema: public; Owner: dungeonasdb
--

CREATE TABLE oggetti_posseduti (
    personaggio integer NOT NULL,
    oggetto integer NOT NULL,
    attivo boolean
);


ALTER TABLE oggetti_posseduti OWNER TO dungeonasdb;

--
-- Name: oggetto; Type: TABLE; Schema: public; Owner: dungeonasdb
--

CREATE TABLE oggetto (
    id integer NOT NULL,
    nome character varying(25) NOT NULL,
    attacco integer,
    difesa integer,
    percezione integer,
    punti_ferita integer,
    valore_mo integer,
    CONSTRAINT mo CHECK ((valore_mo >= 0)),
    CONSTRAINT oggetto_attacco_check CHECK (((attacco >= '-6'::integer) AND (attacco <= 6))),
    CONSTRAINT oggetto_difesa_check CHECK (((difesa >= '-6'::integer) AND (difesa <= 6))),
    CONSTRAINT oggetto_percezione_check CHECK (((percezione >= '-6'::integer) AND (percezione <= 6))),
    CONSTRAINT oggetto_punti_ferita_check CHECK (((punti_ferita >= '-6'::integer) AND (punti_ferita <= 6)))
);


ALTER TABLE oggetto OWNER TO dungeonasdb;

--
-- Name: oggetto_attacco; Type: TABLE; Schema: public; Owner: dungeonasdb
--

CREATE TABLE oggetto_attacco (
    id integer NOT NULL,
    danno integer NOT NULL
);


ALTER TABLE oggetto_attacco OWNER TO dungeonasdb;

--
-- Name: oggetto_consumabile; Type: TABLE; Schema: public; Owner: dungeonasdb
--

CREATE TABLE oggetto_consumabile (
    id integer NOT NULL
);


ALTER TABLE oggetto_consumabile OWNER TO dungeonasdb;

--
-- Name: oggetto_utilizzato_in_stanza; Type: TABLE; Schema: public; Owner: dungeonasdb
--

CREATE TABLE oggetto_utilizzato_in_stanza (
    id integer NOT NULL,
    oggetto integer,
    stanza integer,
    dungeon_giocato integer NOT NULL
);


ALTER TABLE oggetto_utilizzato_in_stanza OWNER TO dungeonasdb;

--
-- Name: oggetto_consumabile_utilizzato_in_stanza_id_seq; Type: SEQUENCE; Schema: public; Owner: dungeonasdb
--

CREATE SEQUENCE oggetto_consumabile_utilizzato_in_stanza_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE oggetto_consumabile_utilizzato_in_stanza_id_seq OWNER TO dungeonasdb;

--
-- Name: oggetto_consumabile_utilizzato_in_stanza_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dungeonasdb
--

ALTER SEQUENCE oggetto_consumabile_utilizzato_in_stanza_id_seq OWNED BY oggetto_utilizzato_in_stanza.id;


--
-- Name: oggetto_difesa; Type: TABLE; Schema: public; Owner: dungeonasdb
--

CREATE TABLE oggetto_difesa (
    id integer NOT NULL
);


ALTER TABLE oggetto_difesa OWNER TO dungeonasdb;

--
-- Name: oggetto_id_seq; Type: SEQUENCE; Schema: public; Owner: dungeonasdb
--

CREATE SEQUENCE oggetto_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE oggetto_id_seq OWNER TO dungeonasdb;

--
-- Name: oggetto_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dungeonasdb
--

ALTER SEQUENCE oggetto_id_seq OWNED BY oggetto.id;


--
-- Name: passaggio; Type: TABLE; Schema: public; Owner: dungeonasdb
--

CREATE TABLE passaggio (
    stanza_provenienza integer,
    stanza_arrivo integer,
    dungeon integer NOT NULL,
    id integer NOT NULL,
    segreto boolean DEFAULT false NOT NULL
);


ALTER TABLE passaggio OWNER TO dungeonasdb;

--
-- Name: passaggio_id_seq; Type: SEQUENCE; Schema: public; Owner: dungeonasdb
--

CREATE SEQUENCE passaggio_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE passaggio_id_seq OWNER TO dungeonasdb;

--
-- Name: passaggio_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dungeonasdb
--

ALTER SEQUENCE passaggio_id_seq OWNED BY passaggio.id;


--
-- Name: personaggio; Type: TABLE; Schema: public; Owner: dungeonasdb
--

CREATE TABLE personaggio (
    id integer NOT NULL,
    nome character varying(20) NOT NULL,
    descrizione character varying(50),
    forza integer NOT NULL,
    intelligenza integer NOT NULL,
    costituzione integer NOT NULL,
    agilita integer NOT NULL,
    esperienza integer DEFAULT 0 NOT NULL,
    monete_oro integer DEFAULT 0 NOT NULL,
    punti_ferita integer,
    utente character varying(50) NOT NULL,
    punti_ferita_iniziali integer,
    CONSTRAINT personaggio_agilita_check CHECK (((agilita > 2) AND (agilita < 18))),
    CONSTRAINT personaggio_costituzione_check CHECK (((costituzione > 2) AND (costituzione < 18))),
    CONSTRAINT personaggio_forza_check CHECK (((forza > 2) AND (forza < 18))),
    CONSTRAINT personaggio_intelligenza_check CHECK (((intelligenza > 2) AND (intelligenza < 18)))
);


ALTER TABLE personaggio OWNER TO dungeonasdb;

--
-- Name: personaggio_id_seq; Type: SEQUENCE; Schema: public; Owner: dungeonasdb
--

CREATE SEQUENCE personaggio_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE personaggio_id_seq OWNER TO dungeonasdb;

--
-- Name: personaggio_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dungeonasdb
--

ALTER SEQUENCE personaggio_id_seq OWNED BY personaggio.id;


--
-- Name: posizione_oggetto; Type: TABLE; Schema: public; Owner: dungeonasdb
--

CREATE TABLE posizione_oggetto (
    oggetto integer NOT NULL,
    visibile boolean NOT NULL,
    stanza integer NOT NULL,
    dungeon integer NOT NULL,
    id integer NOT NULL
);


ALTER TABLE posizione_oggetto OWNER TO dungeonasdb;

--
-- Name: posizione_oggetto_id_seq; Type: SEQUENCE; Schema: public; Owner: dungeonasdb
--

CREATE SEQUENCE posizione_oggetto_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE posizione_oggetto_id_seq OWNER TO dungeonasdb;

--
-- Name: posizione_oggetto_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dungeonasdb
--

ALTER SEQUENCE posizione_oggetto_id_seq OWNED BY posizione_oggetto.id;


--
-- Name: spostamenti_personaggio; Type: TABLE; Schema: public; Owner: dungeonasdb
--

CREATE TABLE spostamenti_personaggio (
    id integer NOT NULL,
    stanza integer NOT NULL,
    attualmente boolean,
    dungeon_giocato integer NOT NULL
);


ALTER TABLE spostamenti_personaggio OWNER TO dungeonasdb;

--
-- Name: spostamenti_personaggio_id_seq; Type: SEQUENCE; Schema: public; Owner: dungeonasdb
--

CREATE SEQUENCE spostamenti_personaggio_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE spostamenti_personaggio_id_seq OWNER TO dungeonasdb;

--
-- Name: spostamenti_personaggio_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dungeonasdb
--

ALTER SEQUENCE spostamenti_personaggio_id_seq OWNED BY spostamenti_personaggio.id;


--
-- Name: stanza; Type: TABLE; Schema: public; Owner: dungeonasdb
--

CREATE TABLE stanza (
    id integer NOT NULL,
    descrizione character varying(20)
);


ALTER TABLE stanza OWNER TO dungeonasdb;

--
-- Name: stanza_id_seq; Type: SEQUENCE; Schema: public; Owner: dungeonasdb
--

CREATE SEQUENCE stanza_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE stanza_id_seq OWNER TO dungeonasdb;

--
-- Name: stanza_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dungeonasdb
--

ALTER SEQUENCE stanza_id_seq OWNED BY stanza.id;


--
-- Name: transazione_id_seq; Type: SEQUENCE; Schema: public; Owner: dungeonasdb
--

CREATE SEQUENCE transazione_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE transazione_id_seq OWNER TO dungeonasdb;

--
-- Name: transazione_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dungeonasdb
--

ALTER SEQUENCE transazione_id_seq OWNED BY transazione.id;


--
-- Name: utente; Type: TABLE; Schema: public; Owner: dungeonasdb
--

CREATE TABLE utente (
    email character varying(50) NOT NULL,
    password character varying(12) NOT NULL,
    id character varying(12)
);


ALTER TABLE utente OWNER TO dungeonasdb;

--
-- Name: attacco id; Type: DEFAULT; Schema: public; Owner: dungeonasdb
--

ALTER TABLE ONLY attacco ALTER COLUMN id SET DEFAULT nextval('attacco_id_seq'::regclass);


--
-- Name: dungeon id; Type: DEFAULT; Schema: public; Owner: dungeonasdb
--

ALTER TABLE ONLY dungeon ALTER COLUMN id SET DEFAULT nextval('dungeon_id_seq'::regclass);


--
-- Name: dungeon pf_gioco; Type: DEFAULT; Schema: public; Owner: dungeonasdb
--

ALTER TABLE ONLY dungeon ALTER COLUMN pf_gioco SET DEFAULT nextval('dungeon_pf_gioco_seq'::regclass);


--
-- Name: dungeon_giocato id; Type: DEFAULT; Schema: public; Owner: dungeonasdb
--

ALTER TABLE ONLY dungeon_giocato ALTER COLUMN id SET DEFAULT nextval('dungeon_giocati_id_seq'::regclass);


--
-- Name: dungeon_giocato terminato; Type: DEFAULT; Schema: public; Owner: dungeonasdb
--

ALTER TABLE ONLY dungeon_giocato ALTER COLUMN terminato SET DEFAULT nextval('dungeon_giocati_terminato_seq'::regclass);


--
-- Name: nemici_sconfitti id; Type: DEFAULT; Schema: public; Owner: dungeonasdb
--

ALTER TABLE ONLY nemici_sconfitti ALTER COLUMN id SET DEFAULT nextval('nemici_sconfitti_id_seq'::regclass);


--
-- Name: nemico id; Type: DEFAULT; Schema: public; Owner: dungeonasdb
--

ALTER TABLE ONLY nemico ALTER COLUMN id SET DEFAULT nextval('nemico_id_seq'::regclass);


--
-- Name: oggetto id; Type: DEFAULT; Schema: public; Owner: dungeonasdb
--

ALTER TABLE ONLY oggetto ALTER COLUMN id SET DEFAULT nextval('oggetto_id_seq'::regclass);


--
-- Name: oggetto_utilizzato_in_stanza id; Type: DEFAULT; Schema: public; Owner: dungeonasdb
--

ALTER TABLE ONLY oggetto_utilizzato_in_stanza ALTER COLUMN id SET DEFAULT nextval('oggetto_consumabile_utilizzato_in_stanza_id_seq'::regclass);


--
-- Name: passaggio id; Type: DEFAULT; Schema: public; Owner: dungeonasdb
--

ALTER TABLE ONLY passaggio ALTER COLUMN id SET DEFAULT nextval('passaggio_id_seq'::regclass);


--
-- Name: personaggio id; Type: DEFAULT; Schema: public; Owner: dungeonasdb
--

ALTER TABLE ONLY personaggio ALTER COLUMN id SET DEFAULT nextval('personaggio_id_seq'::regclass);


--
-- Name: posizione_oggetto id; Type: DEFAULT; Schema: public; Owner: dungeonasdb
--

ALTER TABLE ONLY posizione_oggetto ALTER COLUMN id SET DEFAULT nextval('posizione_oggetto_id_seq'::regclass);


--
-- Name: spostamenti_personaggio id; Type: DEFAULT; Schema: public; Owner: dungeonasdb
--

ALTER TABLE ONLY spostamenti_personaggio ALTER COLUMN id SET DEFAULT nextval('spostamenti_personaggio_id_seq'::regclass);


--
-- Name: stanza id; Type: DEFAULT; Schema: public; Owner: dungeonasdb
--

ALTER TABLE ONLY stanza ALTER COLUMN id SET DEFAULT nextval('stanza_id_seq'::regclass);


--
-- Name: transazione id; Type: DEFAULT; Schema: public; Owner: dungeonasdb
--

ALTER TABLE ONLY transazione ALTER COLUMN id SET DEFAULT nextval('transazione_id_seq'::regclass);


--
-- Data for Name: attacco; Type: TABLE DATA; Schema: public; Owner: dungeonasdb
--

COPY attacco (id, personaggio, nemico, danno, dungeon, stanza) FROM stdin;
\.


--
-- Name: attacco_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dungeonasdb
--

SELECT pg_catalog.setval('attacco_id_seq', 1, false);


--
-- Data for Name: dungeon; Type: TABLE DATA; Schema: public; Owner: dungeonasdb
--

COPY dungeon (id, nome, personaggio, pf_gioco) FROM stdin;
81	CIAO	\N	1
\.


--
-- Name: dungeon_giocati_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dungeonasdb
--

SELECT pg_catalog.setval('dungeon_giocati_id_seq', 1, true);


--
-- Name: dungeon_giocati_terminato_seq; Type: SEQUENCE SET; Schema: public; Owner: dungeonasdb
--

SELECT pg_catalog.setval('dungeon_giocati_terminato_seq', 1, true);


--
-- Data for Name: dungeon_giocato; Type: TABLE DATA; Schema: public; Owner: dungeonasdb
--

COPY dungeon_giocato (dungeon, personaggio, id, terminato) FROM stdin;
\.


--
-- Name: dungeon_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dungeonasdb
--

SELECT pg_catalog.setval('dungeon_id_seq', 81, true);


--
-- Name: dungeon_pf_gioco_seq; Type: SEQUENCE SET; Schema: public; Owner: dungeonasdb
--

SELECT pg_catalog.setval('dungeon_pf_gioco_seq', 1, true);


--
-- Data for Name: nemici_in_stanza; Type: TABLE DATA; Schema: public; Owner: dungeonasdb
--

COPY nemici_in_stanza (nemico, stanza, dungeon, pf_gioco) FROM stdin;
534	36	81	\N
710	28	81	\N
197	28	81	\N
24	14	81	\N
437	14	81	\N
101	33	81	\N
517	33	81	\N
342	33	81	\N
\.


--
-- Data for Name: nemici_sconfitti; Type: TABLE DATA; Schema: public; Owner: dungeonasdb
--

COPY nemici_sconfitti (id, nemico, dungeon_giocato, stanza) FROM stdin;
\.


--
-- Name: nemici_sconfitti_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dungeonasdb
--

SELECT pg_catalog.setval('nemici_sconfitti_id_seq', 1, true);


--
-- Data for Name: nemico; Type: TABLE DATA; Schema: public; Owner: dungeonasdb
--

COPY nemico (id, nome, punti_ferita, danno, attacco, difesa) FROM stdin;
5	CATTIVICK	2	8	3	2
6	JOKER	1	2	1	1
7	ZOMBIE	4	5	3	4
8	DRAGO	4	5	4	3
9	ZOMBIE	1	9	5	4
10	DARKMAN	5	8	2	4
11	Dahaka	1	1	3	2
12	DRAGO	5	3	3	3
13	CALLISTO	9	4	6	9
14	JOKER	12	3	7	10
15	CATTIVICK	8	4	5	6
16	DRAGO	8	3	7	9
17	JOKER	2	8	10	7
18	Torment	14	9	9	10
19	CATTIVICK	5	8	3	3
20	Dahaka	15	7	7	14
21	JOKER	1	7	2	1
22	Dahaka	1	8	1	1
23	CALLISTO	3	7	4	3
24	PICACHU	2	1	2	2
25	ZOMBIE	6	8	2	5
26	DARKMAN	4	6	7	5
27	CALLISTO	3	4	3	3
28	Torment	1	9	6	4
29	DRAGO	11	3	7	10
30	DRAGO	8	6	4	6
31	Dahaka	11	5	9	10
32	Dahaka	9	2	6	10
33	DARKMAN	10	2	14	12
34	ZOMBIE	7	2	7	4
35	DRAGO	10	8	15	12
36	DRAGO	16	5	11	17
37	JOKER	3	3	2	2
38	DRAGO	4	5	3	4
39	CALLISTO	4	5	3	4
40	DARKMAN	4	8	3	3
41	CATTIVICK	5	2	5	4
42	CATTIVICK	5	8	6	6
43	CATTIVICK	8	3	4	5
44	DARKMAN	8	6	7	7
45	DRAGO	2	6	10	6
46	JOKER	8	4	11	9
47	ZOMBIE	6	1	8	5
48	Dahaka	2	5	7	7
49	CATTIVICK	5	6	13	8
50	CALLISTO	4	7	1	2
51	Dahaka	5	2	16	11
52	DRAGO	6	8	5	5
53	CATTIVICK	1	6	1	1
54	CATTIVICK	2	4	2	2
55	CALLISTO	4	6	3	3
56	CALLISTO	6	9	5	5
57	PICACHU	1	1	4	3
58	DRAGO	8	7	2	6
59	CATTIVICK	5	5	3	4
60	JOKER	10	3	7	9
61	CALLISTO	3	6	5	6
62	ZOMBIE	12	9	8	12
63	CALLISTO	3	5	5	4
64	PICACHU	4	2	8	4
65	ZOMBIE	9	1	9	8
66	Torment	11	9	8	6
67	JOKER	16	9	9	14
68	DARKMAN	13	2	6	8
69	DARKMAN	2	1	3	2
70	DRAGO	1	8	2	2
71	ZOMBIE	5	4	3	4
72	PICACHU	2	6	4	4
73	PICACHU	2	5	4	3
74	CALLISTO	2	4	4	4
75	CATTIVICK	5	7	4	3
76	CALLISTO	7	6	5	6
77	DRAGO	5	2	2	4
78	CATTIVICK	3	2	10	7
79	CALLISTO	13	8	9	11
80	Torment	13	4	4	8
81	JOKER	3	3	13	8
82	CATTIVICK	9	2	6	8
83	JOKER	15	1	9	13
84	DRAGO	13	8	11	12
85	JOKER	3	4	1	2
86	PICACHU	4	9	1	2
87	Torment	3	9	2	3
88	ZOMBIE	6	3	3	5
89	CALLISTO	7	2	3	5
90	ZOMBIE	8	2	6	6
91	DARKMAN	2	8	8	5
92	DARKMAN	6	4	8	6
93	ZOMBIE	8	1	8	7
94	Torment	6	6	5	7
95	CALLISTO	13	9	1	7
96	CATTIVICK	1	8	8	7
97	DRAGO	4	8	11	7
98	DRAGO	8	7	6	5
99	PICACHU	16	9	10	11
100	CATTIVICK	3	4	12	10
101	CATTIVICK	1	2	1	1
102	PICACHU	4	5	2	3
103	Torment	5	7	2	4
104	Torment	3	9	2	3
105	Dahaka	7	2	3	4
106	ZOMBIE	8	4	2	5
107	CATTIVICK	9	6	4	6
108	DARKMAN	1	1	6	2
109	CATTIVICK	9	8	4	5
110	JOKER	4	4	8	7
111	CALLISTO	6	8	8	9
112	DRAGO	13	5	10	11
113	DRAGO	3	3	7	4
114	PICACHU	9	1	4	5
115	CALLISTO	4	9	6	3
116	Dahaka	4	8	14	7
117	JOKER	3	7	1	2
118	CATTIVICK	3	5	3	3
119	Dahaka	4	4	3	3
120	DRAGO	5	7	2	3
121	CATTIVICK	4	6	3	3
122	DARKMAN	5	7	5	4
123	CATTIVICK	8	6	4	8
124	CATTIVICK	3	2	7	4
125	CALLISTO	7	9	5	4
126	JOKER	3	5	10	6
127	JOKER	9	3	10	9
128	Dahaka	10	1	7	8
129	Dahaka	8	5	9	9
130	ZOMBIE	10	5	6	7
131	ZOMBIE	9	8	3	7
132	ZOMBIE	13	6	9	10
133	PICACHU	1	3	1	1
134	Dahaka	1	2	3	2
135	Dahaka	3	1	4	3
136	PICACHU	3	5	4	4
137	Dahaka	5	9	5	4
138	CATTIVICK	3	9	4	2
139	CALLISTO	2	4	6	3
140	CATTIVICK	4	9	7	5
141	DRAGO	1	6	1	1
142	DARKMAN	1	8	7	3
143	Torment	12	6	1	6
144	CATTIVICK	7	9	6	8
145	CATTIVICK	3	5	9	4
146	CATTIVICK	12	2	13	13
147	ZOMBIE	13	8	9	14
148	DARKMAN	8	5	11	7
149	CALLISTO	2	4	1	2
150	ZOMBIE	4	1	2	3
151	CATTIVICK	2	8	4	3
152	JOKER	1	7	3	1
153	DARKMAN	2	1	5	2
154	CALLISTO	6	5	2	3
155	DRAGO	3	2	8	5
156	PICACHU	4	4	4	4
157	PICACHU	2	5	6	3
158	ZOMBIE	8	2	6	4
159	PICACHU	11	8	6	9
160	DRAGO	14	9	6	9
161	ZOMBIE	6	2	9	9
162	Torment	12	1	6	12
163	PICACHU	16	6	12	12
164	Torment	8	5	15	11
165	PICACHU	2	5	3	2
166	DRAGO	3	8	4	3
167	DARKMAN	3	5	2	2
168	PICACHU	3	2	4	3
169	Torment	5	3	2	4
170	PICACHU	7	9	3	5
171	Dahaka	6	9	5	6
172	JOKER	9	2	3	7
173	CALLISTO	6	1	7	6
174	Torment	11	8	7	8
175	DARKMAN	5	7	6	4
176	DARKMAN	8	4	7	8
177	Dahaka	13	7	2	7
178	DARKMAN	15	7	8	15
179	ZOMBIE	8	2	8	10
180	CALLISTO	15	1	4	9
181	DARKMAN	2	8	2	2
182	CALLISTO	1	6	2	1
183	DRAGO	5	2	3	4
184	ZOMBIE	3	7	4	4
185	DARKMAN	2	1	6	3
186	CALLISTO	2	3	5	3
187	Torment	5	2	4	5
188	CALLISTO	10	7	5	7
189	CALLISTO	6	6	9	7
190	DARKMAN	6	3	7	8
191	Torment	9	5	4	7
192	JOKER	4	5	3	4
193	CALLISTO	15	1	7	12
194	DRAGO	11	5	8	10
195	CATTIVICK	5	1	6	7
196	ZOMBIE	4	1	12	7
197	CALLISTO	1	1	1	1
198	PICACHU	4	5	2	2
199	Torment	1	2	3	1
200	ZOMBIE	4	2	2	4
201	JOKER	3	9	4	4
202	Torment	5	4	4	3
203	JOKER	9	1	7	8
204	PICACHU	3	7	9	6
205	DRAGO	9	3	5	5
206	Torment	10	4	5	10
207	CATTIVICK	8	4	13	10
208	DRAGO	9	8	2	6
209	JOKER	11	2	8	7
210	CALLISTO	16	7	10	12
211	Torment	10	5	12	11
212	Dahaka	16	4	7	14
213	DARKMAN	3	4	2	3
214	DARKMAN	2	4	3	3
215	Dahaka	2	8	2	1
216	ZOMBIE	4	9	5	5
217	PICACHU	7	8	2	5
218	PICACHU	4	8	5	6
219	CALLISTO	3	8	5	2
220	ZOMBIE	7	3	5	4
221	CALLISTO	6	4	6	4
222	CATTIVICK	7	8	3	5
223	JOKER	13	5	7	11
224	DARKMAN	7	6	8	5
225	PICACHU	15	6	5	10
226	DRAGO	2	6	11	8
227	DRAGO	6	5	8	3
228	Dahaka	9	2	6	10
229	CALLISTO	2	7	2	2
230	JOKER	4	9	2	3
231	ZOMBIE	2	1	2	1
232	PICACHU	4	8	5	4
233	CATTIVICK	2	8	4	3
234	JOKER	1	8	3	2
235	JOKER	4	1	5	4
236	Torment	3	4	5	5
237	CALLISTO	10	9	7	9
238	DRAGO	5	4	8	7
239	JOKER	4	1	9	8
240	DRAGO	4	6	10	6
241	Torment	12	3	8	13
242	CATTIVICK	13	4	3	7
243	DARKMAN	14	2	5	9
244	JOKER	14	6	3	9
245	PICACHU	1	5	1	1
246	JOKER	1	4	2	1
247	JOKER	5	1	3	3
248	DARKMAN	3	7	2	2
249	CATTIVICK	4	6	5	5
250	PICACHU	1	4	4	3
251	DARKMAN	1	7	5	3
252	JOKER	3	7	4	5
253	Dahaka	7	7	7	9
254	Dahaka	2	7	2	2
255	Dahaka	8	2	7	8
256	DRAGO	12	9	6	11
257	DRAGO	14	4	4	9
258	DRAGO	1	3	2	1
259	DRAGO	15	1	10	11
260	Torment	12	1	16	14
261	ZOMBIE	2	7	2	1
262	PICACHU	3	2	3	2
263	DARKMAN	4	7	4	4
264	CATTIVICK	6	9	6	6
265	ZOMBIE	2	6	3	4
266	Dahaka	5	7	2	3
267	ZOMBIE	1	8	5	3
268	CALLISTO	2	5	8	5
269	DARKMAN	10	3	5	8
270	JOKER	11	8	5	10
271	Dahaka	7	1	7	9
272	JOKER	9	5	8	11
273	DRAGO	10	2	7	6
274	JOKER	14	4	8	9
275	PICACHU	16	1	7	10
276	JOKER	9	8	3	5
277	Torment	1	2	3	2
278	CATTIVICK	4	9	2	3
279	Torment	3	2	3	3
280	ZOMBIE	3	9	1	2
281	Dahaka	6	8	7	6
282	JOKER	2	6	2	1
283	DARKMAN	4	1	7	6
284	Torment	7	8	4	4
285	CALLISTO	6	6	4	4
286	DARKMAN	2	2	9	6
287	DARKMAN	13	8	6	7
288	Dahaka	12	4	9	8
289	ZOMBIE	14	9	9	11
290	CATTIVICK	11	1	5	9
291	PICACHU	6	6	9	4
292	ZOMBIE	13	1	17	15
293	Torment	2	5	2	2
294	CALLISTO	4	4	3	3
295	CALLISTO	2	3	2	2
296	Torment	1	2	4	2
297	CATTIVICK	7	3	6	7
298	CALLISTO	3	5	7	5
299	CATTIVICK	4	5	6	6
300	PICACHU	2	3	4	3
301	PICACHU	6	4	4	4
302	ZOMBIE	10	5	3	8
303	ZOMBIE	4	5	4	5
304	JOKER	14	4	6	8
305	DARKMAN	14	7	9	9
306	Torment	14	6	10	10
307	PICACHU	2	9	6	4
308	JOKER	2	8	3	2
309	ZOMBIE	1	4	2	1
310	DRAGO	1	9	4	2
311	JOKER	2	1	3	1
312	ZOMBIE	1	8	6	3
313	Dahaka	6	4	2	4
314	Torment	5	1	5	6
315	Torment	8	6	4	6
316	CATTIVICK	1	8	6	4
317	JOKER	8	2	8	9
318	JOKER	6	6	9	6
319	CATTIVICK	4	3	3	5
320	JOKER	13	8	6	9
321	DARKMAN	5	2	4	5
322	Torment	7	8	13	10
323	DARKMAN	11	5	13	12
324	CALLISTO	3	1	15	7
325	CATTIVICK	2	4	1	1
326	PICACHU	4	8	3	4
327	CATTIVICK	5	4	2	3
328	CALLISTO	1	2	3	3
329	ZOMBIE	6	4	3	6
330	JOKER	4	8	5	5
331	Dahaka	5	6	1	3
332	Torment	3	2	4	3
333	JOKER	7	8	7	8
334	ZOMBIE	12	9	10	11
335	CALLISTO	8	8	11	10
336	Dahaka	2	4	1	1
337	Dahaka	8	6	4	5
338	Dahaka	10	2	8	12
339	CALLISTO	7	5	9	12
340	ZOMBIE	8	1	9	5
341	DRAGO	1	7	1	1
342	PICACHU	2	2	2	1
343	ZOMBIE	3	3	3	4
344	DARKMAN	6	2	2	5
345	DRAGO	4	4	5	5
346	Torment	1	7	4	2
347	PICACHU	5	5	4	4
348	Dahaka	2	6	4	2
349	CATTIVICK	2	8	6	2
350	Torment	1	1	6	1
351	CALLISTO	11	9	7	8
352	PICACHU	12	6	7	10
353	DRAGO	12	7	9	8
354	Torment	2	4	6	3
355	ZOMBIE	3	9	13	9
356	PICACHU	17	1	13	17
357	DARKMAN	1	4	1	1
358	CATTIVICK	3	4	2	3
359	ZOMBIE	3	3	3	3
360	DRAGO	2	1	1	1
361	DARKMAN	5	1	7	6
362	ZOMBIE	8	9	4	4
363	JOKER	5	3	1	3
364	Dahaka	8	5	9	8
365	CATTIVICK	9	8	2	6
366	DRAGO	11	3	3	6
367	Dahaka	12	4	12	11
368	CATTIVICK	6	8	9	9
369	DRAGO	13	1	7	10
370	CATTIVICK	1	5	8	6
371	CATTIVICK	7	8	5	5
372	CATTIVICK	8	6	12	11
373	Dahaka	2	3	1	2
374	CATTIVICK	1	3	4	2
375	DRAGO	5	4	4	4
376	DARKMAN	5	2	3	3
377	ZOMBIE	3	5	3	2
378	CALLISTO	1	3	5	4
379	DARKMAN	3	9	6	3
380	CALLISTO	2	3	1	1
381	ZOMBIE	11	1	2	6
382	Torment	6	5	7	8
383	ZOMBIE	9	4	3	5
384	PICACHU	3	9	13	8
385	DRAGO	5	2	12	10
386	ZOMBIE	1	5	4	3
387	DARKMAN	7	2	6	5
388	ZOMBIE	16	1	12	14
389	CATTIVICK	3	9	1	2
390	DARKMAN	4	2	3	4
391	PICACHU	1	7	3	2
392	PICACHU	4	4	2	3
393	DRAGO	1	4	2	1
394	ZOMBIE	6	6	6	6
395	PICACHU	3	9	1	2
396	PICACHU	6	6	8	7
397	PICACHU	2	5	3	3
398	DRAGO	9	1	7	7
399	Torment	13	3	4	8
400	CALLISTO	10	5	9	10
401	Torment	6	2	11	9
402	ZOMBIE	8	7	6	5
403	DARKMAN	12	6	9	10
404	PICACHU	10	2	10	9
405	CATTIVICK	1	8	2	2
406	CATTIVICK	3	4	3	3
407	CALLISTO	3	5	1	2
408	CALLISTO	3	5	2	2
409	DARKMAN	4	7	5	5
410	PICACHU	6	7	3	4
411	DRAGO	6	9	5	7
412	DRAGO	1	7	4	2
413	DARKMAN	5	6	2	3
414	DRAGO	1	2	7	5
415	CALLISTO	5	1	6	6
416	ZOMBIE	14	6	13	14
417	DRAGO	8	8	8	5
418	Torment	4	9	11	9
419	CATTIVICK	12	8	10	9
420	Torment	8	1	6	4
421	JOKER	3	1	3	3
422	Torment	3	8	3	3
423	DARKMAN	1	2	4	2
424	Dahaka	5	8	4	4
425	Torment	7	3	6	6
426	CATTIVICK	6	9	3	5
427	PICACHU	8	5	4	6
428	DARKMAN	10	2	7	7
429	PICACHU	2	8	4	4
430	DRAGO	11	7	11	11
431	Torment	6	7	7	8
432	DARKMAN	11	1	6	8
433	DRAGO	15	9	2	8
434	JOKER	6	7	7	9
435	CALLISTO	4	6	8	6
436	Torment	10	1	3	6
437	DARKMAN	2	1	2	2
438	DARKMAN	4	6	3	4
439	ZOMBIE	1	9	2	1
440	Dahaka	6	5	2	3
441	CATTIVICK	6	5	3	4
442	PICACHU	1	4	6	4
443	DARKMAN	6	6	4	4
444	DRAGO	8	9	2	5
445	CALLISTO	10	5	3	7
446	Torment	9	3	8	7
447	DRAGO	2	9	2	2
448	Dahaka	12	7	6	11
449	DRAGO	5	2	6	7
450	Torment	16	3	8	12
451	PICACHU	4	9	3	3
452	Dahaka	12	9	2	8
453	DRAGO	1	9	2	2
454	CATTIVICK	4	2	1	3
455	Torment	2	5	1	1
456	JOKER	1	7	5	3
457	JOKER	6	5	2	4
458	DRAGO	2	5	7	5
459	Dahaka	7	1	7	7
460	ZOMBIE	9	7	6	9
461	CATTIVICK	5	5	5	4
462	Torment	6	3	4	4
463	DRAGO	11	6	11	10
464	PICACHU	12	8	6	11
465	CATTIVICK	5	2	11	6
466	Torment	5	5	12	7
467	DARKMAN	13	4	15	15
468	PICACHU	8	6	3	5
469	JOKER	2	2	2	2
470	DRAGO	2	5	3	2
471	PICACHU	2	6	3	2
472	DARKMAN	2	5	1	2
473	CALLISTO	3	7	6	5
474	CATTIVICK	4	9	6	4
475	CATTIVICK	7	8	4	5
476	DRAGO	4	9	6	5
477	ZOMBIE	10	4	11	10
478	Torment	12	2	5	6
479	DARKMAN	7	1	5	8
480	DRAGO	12	7	9	8
481	DRAGO	14	1	6	9
482	CATTIVICK	7	7	11	7
483	DRAGO	12	3	8	9
484	CATTIVICK	12	4	5	10
485	JOKER	3	2	2	3
486	DARKMAN	3	4	2	3
487	DRAGO	2	1	1	1
488	PICACHU	2	6	3	2
489	ZOMBIE	7	4	4	6
490	DRAGO	6	7	2	3
491	PICACHU	3	9	2	2
492	CATTIVICK	2	8	4	2
493	JOKER	8	7	8	6
494	JOKER	8	6	3	6
495	Torment	9	8	9	10
496	DRAGO	9	7	12	11
497	CALLISTO	13	1	10	13
498	JOKER	7	5	10	7
499	CALLISTO	4	1	5	5
500	DRAGO	10	9	9	8
501	Dahaka	1	8	2	1
502	ZOMBIE	2	6	2	1
503	CATTIVICK	2	7	4	3
504	CATTIVICK	6	6	3	4
505	CATTIVICK	4	3	3	2
506	JOKER	3	7	5	2
507	Torment	2	4	3	2
508	CALLISTO	5	1	7	6
509	CATTIVICK	6	3	5	8
510	CALLISTO	8	9	4	4
511	Torment	13	7	2	8
512	Torment	12	2	8	12
513	JOKER	7	5	7	6
514	JOKER	3	5	7	2
515	ZOMBIE	1	6	3	2
516	PICACHU	1	2	10	5
517	Torment	2	1	1	1
518	DRAGO	1	6	2	2
519	JOKER	3	1	2	3
520	Torment	4	3	2	3
521	CATTIVICK	1	7	5	3
522	CATTIVICK	7	7	3	6
523	ZOMBIE	8	9	4	6
524	DRAGO	8	9	9	9
525	DRAGO	3	3	7	6
526	Dahaka	11	1	8	9
527	CALLISTO	3	6	9	5
528	Torment	11	2	6	8
529	CALLISTO	10	9	8	6
530	DARKMAN	3	2	7	5
531	DARKMAN	8	7	12	8
532	CATTIVICK	12	1	4	8
533	DRAGO	3	6	1	2
534	PICACHU	2	2	1	2
535	JOKER	4	9	1	3
536	Torment	1	3	1	1
537	ZOMBIE	3	4	5	5
538	CATTIVICK	6	3	4	5
539	ZOMBIE	3	9	7	6
540	CALLISTO	3	8	4	4
541	JOKER	10	1	6	7
542	Torment	4	2	7	3
543	ZOMBIE	4	2	8	7
544	Torment	3	4	9	7
545	CALLISTO	10	1	14	11
546	ZOMBIE	13	2	10	14
547	DRAGO	8	2	6	7
548	CATTIVICK	15	7	10	12
549	PICACHU	2	5	1	1
550	ZOMBIE	2	4	2	1
551	ZOMBIE	1	1	3	2
552	Torment	6	2	2	4
553	CALLISTO	6	7	2	4
554	ZOMBIE	8	2	4	5
555	DARKMAN	4	1	7	4
556	Torment	2	5	5	2
557	Dahaka	10	8	8	9
558	PICACHU	12	1	2	7
559	PICACHU	13	8	4	7
560	CALLISTO	1	7	12	5
561	CALLISTO	12	4	10	13
562	CATTIVICK	9	8	8	10
563	JOKER	1	2	3	2
564	Torment	13	1	5	7
565	Dahaka	2	2	1	1
566	Torment	2	6	2	2
567	DRAGO	4	5	3	4
568	Dahaka	4	5	6	5
569	ZOMBIE	5	6	3	5
570	DRAGO	2	7	4	1
571	CALLISTO	1	2	6	3
572	DARKMAN	1	5	2	1
573	DARKMAN	10	3	5	8
574	CALLISTO	11	4	10	11
575	CALLISTO	6	5	10	9
576	PICACHU	6	4	6	5
577	JOKER	4	3	4	3
578	Torment	12	4	3	9
579	Dahaka	17	9	7	9
580	ZOMBIE	7	5	8	9
581	Dahaka	1	4	1	1
582	Dahaka	2	2	3	2
583	Dahaka	5	9	2	4
584	CATTIVICK	6	7	5	6
585	ZOMBIE	5	6	5	5
586	JOKER	4	2	6	5
587	Dahaka	2	7	6	4
588	ZOMBIE	3	4	3	2
589	CATTIVICK	10	2	5	6
590	DRAGO	5	3	3	5
591	CALLISTO	10	7	7	6
592	JOKER	11	8	7	8
593	Torment	12	6	8	10
594	PICACHU	10	6	4	8
595	CALLISTO	4	9	2	3
596	PICACHU	4	7	3	5
597	DARKMAN	2	8	1	1
598	Torment	4	8	1	2
599	PICACHU	3	3	2	3
600	DARKMAN	4	2	5	4
601	DARKMAN	1	8	3	3
602	Dahaka	6	3	2	3
603	Dahaka	6	7	5	4
604	Dahaka	6	1	2	4
605	CALLISTO	10	1	6	8
606	CALLISTO	6	3	9	7
607	PICACHU	4	6	11	6
608	DRAGO	12	3	9	10
609	CATTIVICK	6	3	12	8
610	DRAGO	15	2	7	13
611	PICACHU	17	3	8	12
612	ZOMBIE	11	3	15	13
613	DRAGO	1	8	1	1
614	PICACHU	1	6	3	2
615	CATTIVICK	1	6	2	1
616	PICACHU	6	2	3	4
617	CATTIVICK	1	4	3	2
618	CATTIVICK	7	5	7	7
619	PICACHU	4	3	3	3
620	CALLISTO	2	2	9	6
621	Torment	1	8	9	5
622	Dahaka	2	6	10	5
623	DRAGO	11	6	5	9
624	DRAGO	11	5	8	10
625	DRAGO	15	6	9	9
626	Dahaka	2	9	14	7
627	JOKER	2	9	13	8
628	CATTIVICK	14	2	15	16
629	PICACHU	1	8	2	2
630	ZOMBIE	3	5	2	3
631	PICACHU	4	6	2	4
632	CATTIVICK	6	7	3	5
633	ZOMBIE	1	7	4	3
634	PICACHU	1	4	5	3
635	CATTIVICK	4	5	5	3
636	CALLISTO	7	4	4	7
637	Dahaka	7	4	9	7
638	Torment	12	8	10	12
639	Dahaka	5	4	7	4
640	ZOMBIE	12	9	7	12
641	DARKMAN	12	1	8	10
642	PICACHU	1	6	13	8
643	CATTIVICK	17	4	16	16
644	ZOMBIE	15	6	12	13
645	JOKER	2	9	1	2
646	JOKER	1	6	2	2
647	PICACHU	5	4	4	4
648	DARKMAN	6	5	3	5
649	DARKMAN	3	8	3	4
650	DARKMAN	1	8	4	1
651	Torment	8	9	6	5
652	CALLISTO	2	3	2	1
653	JOKER	6	7	7	8
654	DARKMAN	10	5	7	9
655	Dahaka	2	1	10	6
656	ZOMBIE	8	1	8	10
657	JOKER	6	4	12	7
658	CALLISTO	1	8	8	6
659	CALLISTO	13	8	10	10
660	ZOMBIE	13	8	5	8
661	JOKER	2	6	1	2
662	Dahaka	4	2	3	3
663	CALLISTO	5	1	3	4
664	JOKER	4	3	1	3
665	DRAGO	3	2	4	3
666	JOKER	8	5	5	7
667	PICACHU	7	5	1	4
668	DRAGO	2	3	5	3
669	CATTIVICK	10	6	5	9
670	DRAGO	11	6	6	11
671	PICACHU	8	9	10	8
672	DRAGO	6	1	2	3
673	JOKER	6	3	7	7
674	Torment	9	1	12	12
675	PICACHU	16	6	14	14
676	PICACHU	1	8	12	9
677	CALLISTO	1	6	1	1
678	DARKMAN	1	7	2	1
679	CALLISTO	4	9	3	3
680	PICACHU	2	8	2	2
681	Dahaka	5	1	4	3
682	ZOMBIE	5	9	4	6
683	Dahaka	3	8	7	5
684	JOKER	4	8	7	5
685	DARKMAN	1	8	5	1
686	Torment	2	7	5	4
687	DARKMAN	6	7	7	4
688	Torment	13	2	10	10
689	CATTIVICK	11	9	11	11
690	PICACHU	14	7	9	10
691	Dahaka	13	7	9	12
692	DARKMAN	12	2	6	6
693	PICACHU	3	9	2	2
694	CATTIVICK	3	5	3	3
695	Torment	3	1	2	2
696	JOKER	5	1	1	3
697	DARKMAN	7	8	1	4
698	DRAGO	5	2	4	5
699	ZOMBIE	3	2	7	4
700	PICACHU	2	9	5	2
701	CALLISTO	5	2	5	6
702	PICACHU	9	3	5	8
703	JOKER	13	8	8	12
704	DRAGO	13	8	3	9
705	DRAGO	4	2	11	9
706	ZOMBIE	5	1	12	10
707	JOKER	9	9	7	9
708	Torment	4	5	15	9
709	DRAGO	1	4	2	1
710	DRAGO	3	1	1	2
711	DRAGO	5	3	2	3
712	DRAGO	3	8	2	3
713	DRAGO	1	2	4	3
714	DARKMAN	7	2	5	7
715	Dahaka	7	6	4	6
716	CATTIVICK	9	4	5	9
717	JOKER	8	5	7	7
718	DARKMAN	1	1	9	5
719	Dahaka	2	4	8	3
720	DRAGO	11	3	9	9
721	CALLISTO	4	8	5	3
722	Torment	1	1	5	3
723	ZOMBIE	12	4	8	13
724	Dahaka	1	5	5	5
725	ZOMBIE	3	9	2	3
726	Dahaka	3	6	3	3
727	JOKER	3	5	3	2
728	DARKMAN	2	8	4	3
729	DARKMAN	3	1	4	3
730	JOKER	2	4	2	3
731	CATTIVICK	8	4	6	6
732	PICACHU	6	8	2	4
733	CATTIVICK	6	6	6	4
734	DRAGO	3	2	9	7
735	JOKER	12	4	2	6
736	PICACHU	14	2	10	14
737	DRAGO	8	7	9	11
738	PICACHU	5	2	6	8
739	DARKMAN	12	5	7	7
740	CALLISTO	8	4	2	6
741	CALLISTO	1	6	2	2
742	ZOMBIE	3	5	3	3
743	Torment	2	3	4	3
744	Dahaka	1	6	2	1
745	DRAGO	4	4	3	5
746	PICACHU	6	9	3	5
747	ZOMBIE	4	3	5	3
748	ZOMBIE	8	7	2	5
749	CALLISTO	8	8	4	7
750	ZOMBIE	10	9	11	10
751	DRAGO	5	5	10	8
752	DARKMAN	10	2	4	8
753	Dahaka	10	1	13	10
754	PICACHU	6	7	12	10
755	DARKMAN	10	3	6	10
756	Dahaka	2	3	13	5
757	PICACHU	3	1	2	3
758	Torment	3	5	3	3
759	DARKMAN	1	3	3	2
760	CATTIVICK	4	9	3	4
761	CALLISTO	1	2	5	2
762	PICACHU	8	1	4	4
763	Dahaka	9	7	4	5
764	JOKER	5	2	8	5
765	CALLISTO	4	2	1	3
766	JOKER	3	2	4	5
767	ZOMBIE	6	2	6	4
768	Dahaka	6	3	8	5
769	ZOMBIE	2	2	4	3
770	DRAGO	10	4	8	13
771	CATTIVICK	13	9	14	12
772	JOKER	13	1	15	14
773	PICACHU	2	9	2	2
774	ZOMBIE	2	6	1	1
775	CALLISTO	4	7	3	4
776	JOKER	5	3	3	5
777	PICACHU	6	5	4	6
778	JOKER	3	3	6	4
779	DARKMAN	5	3	6	7
780	ZOMBIE	9	3	7	8
781	DARKMAN	11	8	2	7
782	CATTIVICK	5	7	10	8
783	DARKMAN	10	3	9	8
784	DARKMAN	6	8	4	7
785	Torment	15	3	7	13
786	Dahaka	11	5	7	11
787	JOKER	5	7	10	9
788	DRAGO	2	3	13	9
789	Torment	3	6	2	3
790	Dahaka	1	4	1	1
791	PICACHU	5	2	1	3
792	ZOMBIE	4	6	5	5
793	PICACHU	7	2	4	7
794	PICACHU	7	6	3	6
795	JOKER	1	2	3	2
796	ZOMBIE	4	2	2	3
797	CATTIVICK	10	2	6	7
798	DARKMAN	5	9	6	5
799	Dahaka	4	6	10	6
800	CATTIVICK	2	3	4	4
\.


--
-- Name: nemico_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dungeonasdb
--

SELECT pg_catalog.setval('nemico_id_seq', 804, true);


--
-- Data for Name: oggetti_posseduti; Type: TABLE DATA; Schema: public; Owner: dungeonasdb
--

COPY oggetti_posseduti (personaggio, oggetto, attivo) FROM stdin;
\.


--
-- Data for Name: oggetto; Type: TABLE DATA; Schema: public; Owner: dungeonasdb
--

COPY oggetto (id, nome, attacco, difesa, percezione, punti_ferita, valore_mo) FROM stdin;
2030	0A	-6	0	5	-6	\N
2032	1A	-6	0	-5	4	\N
2034	2A	-3	0	-1	-3	\N
2036	3A	5	0	3	0	\N
2038	4A	-5	0	-3	2	\N
2040	5A	2	0	-6	1	\N
2042	6A	-3	0	-4	-1	\N
2044	7C	3	5	1	-4	\N
2046	8A	1	0	-4	-2	\N
2048	9A	-2	0	-3	2	\N
2050	10A	4	0	-1	4	\N
2052	11A	-1	0	4	2	\N
2054	12A	2	0	-2	-2	\N
2056	13A	5	0	3	-5	\N
2058	14A	-5	0	-2	-1	\N
2060	15A	-3	0	-3	3	\N
2062	16A	5	0	-2	3	\N
2064	17A	-4	0	0	1	\N
2066	18A	-4	0	2	-6	\N
2068	19A	0	0	0	4	\N
2070	20A	4	0	-4	-5	\N
2072	21C	-3	0	4	2	\N
2074	22D	3	4	-4	-2	\N
2076	23A	-2	0	-2	-2	\N
2078	24C	4	5	-2	3	\N
2080	25A	1	0	-6	3	\N
2082	26A	-6	0	3	3	\N
2084	27C	0	-3	2	5	\N
2086	28C	1	4	2	-6	\N
2088	29D	4	1	0	-4	\N
2090	30D	1	3	-5	-1	\N
2092	31C	1	3	-3	-2	\N
2094	32A	2	0	3	-4	\N
2096	33D	-6	3	0	-2	\N
2098	34A	0	0	1	0	\N
2100	35A	4	0	2	-6	\N
2102	36A	2	0	-1	-6	\N
2104	37A	2	0	-5	5	\N
2106	38A	5	0	-5	-2	\N
2108	39C	5	0	-6	0	\N
2110	40D	3	4	-1	-1	\N
2112	41A	2	0	-6	2	\N
2114	42A	-1	0	-1	-2	\N
2116	43A	-2	0	-5	2	\N
2118	44A	2	0	1	-4	\N
2120	45C	-6	-1	2	-1	\N
2122	46A	-5	0	3	-5	\N
2124	47A	0	0	4	-6	\N
2126	48A	-6	0	3	-4	\N
2128	49A	4	0	-5	-1	\N
2130	50C	3	4	-5	-1	\N
2132	51A	-1	0	3	-4	\N
2134	52C	1	-5	-6	-1	\N
2136	53C	-2	-4	5	-5	\N
2138	54A	4	0	-2	-6	\N
2140	55D	2	3	-6	4	\N
2142	56C	-1	3	-3	2	\N
2144	57A	-3	0	-6	3	\N
2146	58C	-5	3	3	-3	\N
2148	59D	-4	5	5	-4	\N
2150	60A	4	0	3	5	\N
2152	61A	-1	0	1	-5	\N
2154	62A	-1	0	1	-4	\N
2156	63A	-2	0	-4	2	\N
2158	64A	4	0	2	-5	\N
2160	65A	-6	0	-6	4	\N
2162	66C	-5	-6	4	2	\N
2164	67A	-3	0	3	5	\N
2166	68C	-1	-3	-5	1	\N
2168	69D	-1	1	-2	-4	\N
2170	70C	-5	4	-5	5	\N
2172	71A	-5	0	1	4	\N
2174	72C	-5	-1	-6	-5	\N
2176	73A	0	0	2	-4	\N
2178	74A	1	0	-6	4	\N
2180	75A	5	0	-5	-6	\N
2182	76A	3	0	-5	-2	\N
2184	77A	1	0	-2	1	\N
2186	78A	-4	0	-2	4	\N
2188	79D	0	5	1	1	\N
2190	80A	5	0	5	-6	\N
2192	81A	3	0	2	-4	\N
2194	82A	4	0	-6	0	\N
2196	83D	2	1	-6	1	\N
2198	84A	1	0	3	2	\N
2200	85D	2	3	1	2	\N
2202	86A	2	0	2	1	\N
2204	87C	-3	1	-5	4	\N
2206	88C	2	-2	-1	-4	\N
2208	89D	4	2	-2	0	\N
2210	90A	4	0	4	-4	\N
2212	91C	5	3	-5	0	\N
2214	92C	4	5	3	2	\N
2216	93C	-1	1	-5	1	\N
2218	94D	0	2	-6	-6	\N
2220	95A	-6	0	-4	-3	\N
2222	96A	-4	0	2	3	\N
2224	97A	2	0	-3	-1	\N
2226	98A	4	0	5	0	\N
2228	99C	-6	4	3	-5	\N
2230	100D	-2	2	1	1	\N
2232	101D	2	1	5	-1	\N
2234	102A	5	0	1	5	\N
2236	103D	0	1	-6	-4	\N
2238	104A	-6	0	-4	4	\N
2240	105C	-1	-4	4	1	\N
2242	106A	-5	0	-6	-1	\N
2244	107D	-2	5	3	-2	\N
2246	108D	1	5	-5	4	\N
2248	109A	1	0	2	-4	\N
2250	110C	-1	2	3	4	\N
2252	111C	-3	-3	-2	-5	\N
2254	112A	1	0	-5	1	\N
2256	113D	-4	5	-5	-5	\N
2258	114A	4	0	2	-5	\N
2260	115A	0	0	2	-1	\N
2262	116C	2	3	-6	3	\N
2264	117D	-2	2	-6	-6	\N
2266	118D	-6	2	-2	-2	\N
2268	119A	-3	0	-2	4	\N
2270	120D	-4	3	-2	3	\N
2272	121D	-2	2	3	-4	\N
2274	122D	-3	5	-3	0	\N
2276	123D	3	3	-3	-6	\N
2278	124A	-6	0	-2	-3	\N
2280	125A	-6	0	2	1	\N
2282	126A	-1	0	4	3	\N
2284	127A	-4	0	2	2	\N
2286	128A	0	0	5	1	\N
2288	129D	-6	1	-1	-2	\N
2290	130D	-5	1	3	2	\N
2292	131D	2	5	5	2	\N
2294	132D	-5	4	0	3	\N
2296	133A	-5	0	5	-4	\N
2298	134A	0	0	4	1	\N
2300	135A	-1	0	-2	3	\N
2302	136A	-6	0	3	-1	\N
2304	137D	0	1	-3	-3	\N
2306	138A	-3	0	-6	-1	\N
2308	139C	-6	-2	-2	2	\N
2310	140D	-5	5	-6	5	\N
2312	141A	3	0	-2	0	\N
2314	142D	-3	4	-4	-1	\N
2316	143A	1	0	-4	-2	\N
2318	144A	3	0	4	4	\N
2320	145A	0	0	5	3	\N
2322	146D	0	3	-2	-5	\N
2324	147A	-2	0	4	3	\N
2326	148A	-2	0	3	3	\N
2328	149A	0	0	4	-1	\N
2330	150C	5	0	5	2	\N
2332	151A	-3	0	-3	-1	\N
2334	152A	4	0	-5	2	\N
2336	153C	-3	-3	5	-6	\N
2338	154D	-6	5	2	5	\N
2340	155C	5	5	3	2	\N
2342	156C	5	0	5	-2	\N
2344	157C	-4	-3	1	5	\N
2346	158D	-4	2	0	-4	\N
2348	159D	-6	5	-4	-6	\N
2350	160A	-2	0	2	2	\N
2352	161D	-4	4	-6	-2	\N
2354	162D	3	5	3	-3	\N
2356	163A	2	0	-5	5	\N
2358	164A	-4	0	1	-2	\N
2360	165A	-3	0	1	5	\N
2362	166C	0	-6	-3	2	\N
2364	167A	-1	0	2	3	\N
2366	168A	1	0	-1	3	\N
2368	169D	5	3	2	0	\N
2370	170A	-1	0	-5	-2	\N
2372	171A	4	0	-4	0	\N
2374	172C	-6	-1	4	3	\N
2376	173A	-1	0	-3	-4	\N
2378	174A	-4	0	-6	2	\N
2380	175C	-4	5	5	-3	\N
2382	176A	-3	0	-5	0	\N
2384	177A	2	0	-1	1	\N
2386	178D	-2	1	4	2	\N
2388	179D	-1	2	-6	-4	\N
2390	180A	-1	0	2	3	\N
2392	181D	5	3	1	3	\N
2394	182D	0	2	-4	-2	\N
2396	183A	-6	0	2	-6	\N
2398	184A	-5	0	-6	0	\N
2400	185D	5	5	-4	4	\N
2402	186A	4	0	-5	-4	\N
2404	187A	-4	0	-6	0	\N
2406	188D	1	5	4	2	\N
2408	189A	0	0	-2	-5	\N
2410	190D	-2	3	-4	1	\N
2412	191C	-2	-2	3	4	\N
2414	192D	1	5	-1	0	\N
2416	193A	-4	0	-2	1	\N
2418	194D	-5	1	-5	-6	\N
2420	195A	0	0	0	3	\N
2422	196D	-4	2	2	-4	\N
2424	197A	4	0	-1	5	\N
2426	198D	3	2	-3	-2	\N
2428	199A	-3	0	2	3	\N
2430	200A	5	0	-5	1	\N
2432	201A	-6	0	-1	-1	\N
2434	202A	-3	0	-5	-3	\N
2436	203D	-6	5	-6	-1	\N
2438	204C	-2	3	0	-4	\N
2440	205D	-5	5	5	3	\N
2442	206C	5	2	-3	4	\N
2444	207C	2	0	4	-2	\N
2446	208A	-4	0	-1	-3	\N
2448	209C	2	2	3	5	\N
2450	210A	-6	0	1	4	\N
2452	211A	2	0	2	5	\N
2454	212A	0	0	2	-1	\N
2456	213D	-1	1	-1	4	\N
2458	214C	4	-6	5	3	\N
2460	215D	4	2	-3	2	\N
2462	216A	4	0	-2	-2	\N
2464	217A	1	0	-2	-4	\N
2466	218A	-6	0	1	-4	\N
2468	219A	-6	0	2	3	\N
2470	220C	2	-6	0	-2	\N
2472	221C	2	-5	0	-5	\N
2474	222C	-6	-5	-5	-2	\N
2476	223A	-5	0	5	3	\N
2478	224A	0	0	-6	-4	\N
2480	225A	-1	0	1	-6	\N
2482	226D	2	5	5	-3	\N
2484	227A	-2	0	-6	3	\N
2486	228D	5	2	3	2	\N
2488	229A	3	0	-1	-3	\N
2490	230C	-1	0	5	-6	\N
2492	231A	-3	0	5	2	\N
2494	232C	-5	-2	4	4	\N
2496	233A	3	0	0	0	\N
2498	234A	3	0	-5	-4	\N
2500	235D	-5	4	1	-6	\N
2502	236C	2	-2	2	-2	\N
2504	237A	-1	0	-2	-2	\N
2506	238A	-5	0	0	2	\N
2508	239C	-1	-5	0	1	\N
2510	240A	3	0	-3	3	\N
2512	241A	0	0	2	-4	\N
2514	242A	2	0	-2	-5	\N
2516	243D	-4	1	4	3	\N
2518	244A	5	0	2	0	\N
2520	245A	-2	0	2	-5	\N
2522	246C	1	-4	2	4	\N
2524	247A	1	0	5	2	\N
2526	248D	4	5	-6	1	\N
2528	249C	-5	5	0	2	\N
2530	250C	-6	-1	2	-4	\N
2532	251C	4	-2	-2	2	\N
2534	252A	-3	0	4	-6	\N
2536	253A	4	0	-6	0	\N
2538	254C	-2	2	-2	4	\N
2540	255A	4	0	-3	0	\N
2542	256D	-4	3	4	1	\N
2544	257D	-1	2	-2	-1	\N
2546	258A	3	0	4	-2	\N
2548	259C	-6	-3	-5	-2	\N
2550	260A	3	0	0	-1	\N
2552	261C	4	3	-2	-6	\N
2554	262C	0	4	-3	-6	\N
2556	263D	-2	3	-5	-3	\N
2558	264A	0	0	1	4	\N
2560	265A	2	0	-5	-5	\N
2562	266A	-6	0	-4	-5	\N
2564	267D	1	1	-1	1	\N
2566	268C	-4	-3	-3	2	\N
2568	269D	-4	5	-1	-5	\N
2570	270D	-3	1	5	-4	\N
2572	271A	-3	0	4	-1	\N
2574	272C	4	-1	1	-6	\N
2576	273A	-3	0	1	1	\N
2578	274C	-2	-6	5	1	\N
2580	275C	4	-3	3	-5	\N
2582	276C	-5	2	4	5	\N
2584	277C	0	-6	-6	-4	\N
2586	278A	-5	0	-3	3	\N
2588	279A	1	0	5	1	\N
2590	280A	-2	0	-3	-5	\N
2592	281C	4	-5	-2	3	\N
2594	282A	-2	0	0	-1	\N
2596	283A	5	0	4	-3	\N
2598	284D	-5	1	-4	-5	\N
2600	285D	-1	3	-1	1	\N
2602	286C	-4	0	-3	-3	\N
2604	287D	-6	1	-4	5	\N
2606	288A	5	0	5	4	\N
2608	289A	-6	0	1	-4	\N
2610	290D	-3	3	2	-2	\N
2612	291C	5	0	4	-6	\N
2614	292A	1	0	4	2	\N
2616	293A	2	0	5	0	\N
2618	294D	1	1	-6	3	\N
2620	295C	-3	4	-1	2	\N
2622	296C	2	2	3	2	\N
2624	297D	-3	4	0	-6	\N
2626	298A	-5	0	-3	2	\N
2628	299C	5	5	3	5	\N
2630	300D	-3	4	-3	-6	\N
2632	301A	3	0	-1	3	\N
2634	302A	-3	0	3	-2	\N
2636	303C	0	-4	4	-3	\N
2638	304D	-4	3	0	-4	\N
2640	305D	-6	5	-1	-6	\N
2642	306C	-5	2	0	4	\N
2644	307A	-5	0	-3	5	\N
2646	308A	-5	0	-5	-4	\N
2648	309A	-4	0	0	-3	\N
2650	310C	-3	-3	-1	-3	\N
2652	311C	-5	-1	-2	2	\N
2654	312C	0	0	-5	4	\N
2656	313A	1	0	2	3	\N
2658	314C	-5	5	-2	1	\N
2660	315A	1	0	-4	-5	\N
2662	316A	4	0	-6	-4	\N
2664	317D	-5	1	5	-4	\N
2666	318A	2	0	4	-1	\N
2668	319A	0	0	-1	-5	\N
2670	320D	3	4	1	4	\N
2672	321A	5	0	5	0	\N
2674	322D	2	3	-4	2	\N
2676	323D	-6	3	4	3	\N
2678	324C	4	1	-3	-3	\N
2680	325A	5	0	0	-4	\N
2682	326A	0	0	-6	-4	\N
2684	327A	5	0	0	-6	\N
2686	328D	-5	2	4	4	\N
2688	329D	-4	3	1	0	\N
2690	330D	0	3	2	-1	\N
2692	331C	-3	0	-1	-6	\N
2694	332A	1	0	-2	0	\N
2696	333A	-2	0	1	1	\N
2698	334A	0	0	-1	2	\N
2700	335A	-4	0	2	2	\N
2702	336C	-4	5	-1	1	\N
2704	337C	-3	1	-2	1	\N
2706	338D	5	3	-1	0	\N
2708	339A	2	0	4	-1	\N
2710	340A	2	0	0	-1	\N
2712	341A	1	0	5	-3	\N
2714	342D	1	2	-3	-3	\N
2716	343D	-6	5	3	-3	\N
2718	344A	5	0	-1	0	\N
2720	345A	-1	0	2	-6	\N
2722	346A	1	0	1	-4	\N
2724	347C	0	-6	-1	5	\N
2726	348C	-6	-5	0	-3	\N
2728	349A	-3	0	4	3	\N
2730	350D	5	5	-4	1	\N
2732	351A	4	0	1	-5	\N
2734	352D	1	4	-3	1	\N
2736	353D	-3	5	5	0	\N
2738	354C	-4	-6	1	5	\N
2740	355A	-4	0	-6	-3	\N
2742	356C	-4	-3	-6	5	\N
2744	357A	2	0	-4	4	\N
2746	358D	-5	3	4	1	\N
2748	359C	1	-5	-6	0	\N
2750	360A	2	0	5	-6	\N
2752	361A	-3	0	4	-4	\N
2754	362A	5	0	2	3	\N
2756	363C	4	5	-1	0	\N
2758	364A	-6	0	1	-5	\N
2760	365D	5	4	1	5	\N
2762	366A	0	0	1	3	\N
2764	367D	3	3	4	0	\N
2766	368D	-5	3	-6	-1	\N
2768	369D	-6	3	-6	4	\N
2770	370D	-6	3	0	3	\N
2772	371A	-6	0	1	3	\N
2774	372A	-2	0	5	3	\N
2776	373A	3	0	-4	4	\N
2778	374C	5	3	-5	-6	\N
2780	375C	-5	3	-5	5	\N
2782	376A	-2	0	5	-6	\N
2784	377C	-1	-3	-4	-1	\N
2786	378A	-4	0	-4	-1	\N
2788	379A	2	0	2	4	\N
2790	380A	-2	0	1	0	\N
2792	381C	-4	-3	3	-5	\N
2794	382D	2	4	5	4	\N
2796	383A	-1	0	5	4	\N
2798	384D	1	5	1	-1	\N
2800	385A	-4	0	-2	-5	\N
2802	386D	-2	3	3	1	\N
2804	387D	-3	2	1	-2	\N
2806	388A	-6	0	-5	5	\N
2808	389A	0	0	5	5	\N
2810	390A	-6	0	-3	3	\N
2812	391A	-2	0	-3	1	\N
2814	392D	5	4	-3	-3	\N
2816	393A	-3	0	4	-4	\N
2818	394A	2	0	-1	2	\N
2820	395C	2	2	5	-1	\N
2822	396A	4	0	4	-1	\N
2824	397D	-1	3	4	3	\N
2826	398C	2	0	-3	5	\N
2828	399A	2	0	-3	-2	\N
2830	400A	-5	0	5	0	\N
2832	401A	-1	0	-1	4	\N
2834	402D	-2	3	5	-5	\N
2836	403C	4	2	4	3	\N
2838	404A	-1	0	4	4	\N
2840	405A	5	0	3	0	\N
2842	406A	5	0	3	4	\N
2844	407A	-4	0	2	-3	\N
2846	408C	4	0	1	2	\N
2848	409A	-3	0	3	-4	\N
2850	410C	-6	1	-3	4	\N
2852	411A	-4	0	-5	5	\N
2854	412D	3	3	-1	0	\N
2856	413C	-4	2	-5	4	\N
2858	414C	-4	3	-1	-1	\N
2860	415D	-1	3	-4	-2	\N
2862	416D	-4	2	0	-5	\N
2864	417C	2	4	1	4	\N
2866	418C	0	-1	3	0	\N
2868	419A	-2	0	-1	3	\N
2870	420A	3	0	-3	5	\N
2872	421C	-4	-4	-3	-3	\N
2874	422A	0	0	0	-5	\N
2876	423C	-6	0	-2	3	\N
2878	424C	3	-2	-3	-4	\N
2880	425A	-5	0	2	-2	\N
2882	426D	-4	1	4	-1	\N
2884	427C	5	-2	2	-5	\N
2886	428A	-4	0	-6	5	\N
2888	429C	2	-3	4	5	\N
2890	430D	-6	4	-1	-1	\N
2892	431D	1	1	-5	-6	\N
2894	432A	1	0	-4	-6	\N
2896	433D	5	2	-2	4	\N
2898	434D	-4	3	2	-6	\N
2900	435C	0	1	0	-5	\N
2902	436D	-4	3	2	0	\N
2904	437C	1	-1	-5	-6	\N
2906	438D	-2	3	-4	-1	\N
2908	439A	-4	0	1	-2	\N
2910	440A	-6	0	-3	-3	\N
2912	441C	1	2	-5	-6	\N
2914	442A	1	0	3	1	\N
2916	443A	-6	0	3	-1	\N
2918	444C	-6	4	2	1	\N
2920	445A	4	0	-5	-4	\N
2922	446C	2	-3	3	3	\N
2924	447A	-1	0	-1	-4	\N
2926	448D	3	5	0	1	\N
2928	449D	-4	4	1	4	\N
2930	450C	1	-3	4	-3	\N
2932	451C	0	-5	-1	5	\N
2934	452A	5	0	-6	0	\N
2936	453D	-3	4	-1	-1	\N
2938	454D	-1	2	-1	2	\N
2940	455A	-2	0	1	2	\N
2942	456A	4	0	-5	4	\N
2944	457A	-3	0	1	-6	\N
2946	458A	-3	0	-3	2	\N
2948	459A	-4	0	-1	4	\N
2950	460A	-3	0	1	4	\N
2952	461D	-5	2	-2	-4	\N
2954	462C	1	-3	4	1	\N
2956	463A	5	0	-5	2	\N
2958	464A	4	0	-1	3	\N
2960	465C	-6	3	2	5	\N
2962	466C	-6	4	-2	-4	\N
2964	467A	0	0	5	-2	\N
2966	468D	-4	2	-1	-3	\N
2968	469A	-6	0	4	4	\N
2970	470A	4	0	1	3	\N
2972	471A	4	0	5	-5	\N
2974	472C	1	2	1	-6	\N
2976	473A	-4	0	-4	0	\N
2978	474A	4	0	-3	3	\N
2980	475A	1	0	-4	-3	\N
2982	476C	4	-2	1	3	\N
2984	477C	-2	-5	-3	-6	\N
2986	478A	2	0	-3	5	\N
2988	479A	-1	0	-5	3	\N
2990	480D	1	5	2	4	\N
2992	481D	-3	3	0	-5	\N
2994	482A	-4	0	0	-2	\N
2996	483C	1	-5	-5	0	\N
2998	484D	-3	5	-2	3	\N
3000	485A	-4	0	-1	-1	\N
3002	486D	-5	3	3	-3	\N
3004	487C	2	3	4	0	\N
3006	488A	1	0	-6	5	\N
3008	489D	-2	1	-5	4	\N
3010	490C	-4	0	-1	-6	\N
3012	491D	-2	2	4	2	\N
3014	492A	-4	0	5	-3	\N
3016	493C	-1	3	0	-3	\N
3018	494C	0	-4	2	4	\N
3020	495C	-5	4	-4	-5	\N
3022	496A	-1	0	1	-4	\N
3024	497C	0	3	-4	-1	\N
3026	498A	-6	0	2	0	\N
3028	499D	-1	5	-6	-4	\N
3030	500C	-2	-4	-4	0	\N
3032	501C	5	0	1	-4	\N
3034	502C	2	5	3	-4	\N
3036	503D	-3	2	-2	4	\N
3038	504A	3	0	0	-6	\N
3040	505C	-1	2	1	5	\N
3042	506A	4	0	-4	-6	\N
3044	507A	3	0	1	-6	\N
3046	508A	-3	0	1	-4	\N
3048	509A	-1	0	5	5	\N
3050	510D	-1	4	-3	-5	\N
3052	511A	5	0	5	-6	\N
3054	512A	4	0	-2	5	\N
3056	513A	-3	0	-3	-1	\N
3058	514C	4	1	-6	4	\N
3060	515A	-2	0	4	-1	\N
3062	516A	-5	0	-4	-5	\N
3064	517A	4	0	2	-4	\N
3066	518D	3	1	-2	-6	\N
3068	519C	-5	5	-4	-6	\N
3070	520A	-2	0	1	3	\N
3072	521A	5	0	-6	-6	\N
3074	522D	4	4	3	-6	\N
3076	523A	3	0	4	-6	\N
3078	524A	-5	0	-5	-5	\N
3080	525D	-1	1	0	-4	\N
3082	526C	1	-2	-5	0	\N
3084	527C	-6	-4	-6	-5	\N
3086	528A	4	0	1	5	\N
3088	529C	-6	-4	3	-5	\N
3090	530A	1	0	-3	4	\N
3092	531A	5	0	5	-5	\N
3094	532A	-5	0	1	-5	\N
3096	533A	5	0	3	1	\N
3098	534A	-4	0	0	5	\N
3100	535D	2	3	3	5	\N
3102	536C	-3	-1	0	-2	\N
3104	537A	0	0	1	2	\N
3106	538C	1	-1	-2	-4	\N
3108	539A	-2	0	-3	-2	\N
3110	540D	-6	3	3	-6	\N
3112	541A	3	0	-5	-4	\N
3114	542D	2	1	2	-2	\N
3116	543A	3	0	-4	5	\N
3118	544A	4	0	-6	0	\N
3120	545A	3	0	-6	-3	\N
3122	546C	-6	-2	3	-4	\N
3124	547A	5	0	1	-2	\N
3126	548A	-2	0	3	1	\N
3128	549C	-3	1	-5	-3	\N
3130	550D	-3	1	0	-2	\N
3132	551A	-3	0	4	0	\N
3134	552D	0	2	-1	4	\N
3136	553A	-3	0	5	-6	\N
3138	554A	1	0	-1	3	\N
3140	555A	3	0	-4	-6	\N
3142	556C	-3	-3	-5	4	\N
3144	557C	-2	1	-3	-3	\N
3146	558C	1	-2	-6	0	\N
3148	559A	3	0	-1	-3	\N
3150	560A	-6	0	-2	-3	\N
3152	561A	0	0	-2	1	\N
3154	562A	0	0	-5	4	\N
3156	563A	1	0	1	2	\N
3158	564A	-2	0	-6	4	\N
3160	565D	-6	3	1	-1	\N
3162	566D	1	5	3	-3	\N
3164	567A	-2	0	2	4	\N
3166	568C	-6	-3	-5	3	\N
3168	569D	-3	5	-2	-5	\N
3170	570C	4	3	-5	-5	\N
3172	571C	-2	-5	-6	-1	\N
3174	572A	4	0	1	2	\N
3176	573C	3	3	-4	0	\N
3178	574A	4	0	-5	5	\N
3180	575A	5	0	-2	-6	\N
3182	576A	3	0	2	4	\N
3184	577C	3	-1	-6	-1	\N
3186	578A	-1	0	-3	1	\N
3188	579C	-2	-1	3	-1	\N
3190	580C	5	-2	-5	-2	\N
3192	581D	5	5	4	2	\N
3194	582D	4	4	4	-3	\N
3196	583C	-6	2	-2	1	\N
3198	584D	-6	5	-4	0	\N
3200	585A	0	0	-2	-2	\N
3202	586C	-3	-3	2	2	\N
3204	587C	1	5	3	2	\N
3206	588C	3	-5	5	-2	\N
3208	589A	-1	0	5	5	\N
3210	590A	4	0	3	-4	\N
3212	591C	4	3	1	0	\N
3214	592A	-5	0	2	4	\N
3216	593A	0	0	-5	-1	\N
3218	594D	-4	3	-1	-4	\N
3220	595A	-4	0	-4	-1	\N
3222	596C	-3	0	-5	3	\N
3224	597A	4	0	0	3	\N
3226	598D	-3	1	3	3	\N
3228	599A	4	0	-3	-4	\N
3230	600A	-2	0	4	4	\N
3232	601D	-5	5	1	5	\N
3234	602A	1	0	2	1	\N
3236	603A	-4	0	3	2	\N
3238	604A	0	0	-5	3	\N
3240	605A	-4	0	2	-6	\N
3242	606A	-4	0	2	-5	\N
3244	607A	-6	0	-5	2	\N
3246	608A	2	0	-4	0	\N
3248	609C	-2	-3	5	0	\N
3250	610C	-2	-4	4	-6	\N
3252	611A	-5	0	1	4	\N
3254	612D	-6	5	-2	-4	\N
3256	613A	-6	0	-4	1	\N
3258	614C	-6	-5	5	0	\N
3260	615A	4	0	1	4	\N
3262	616A	-6	0	4	5	\N
3264	617A	5	0	-2	-5	\N
3266	618C	-5	1	2	-3	\N
3268	619A	-3	0	2	-4	\N
3270	620A	-6	0	-4	2	\N
3272	621A	4	0	5	3	\N
3274	622A	3	0	4	-4	\N
3276	623D	2	1	-5	-1	\N
3278	624C	-2	0	2	1	\N
3280	625A	1	0	3	-3	\N
3282	626A	-4	0	1	5	\N
3284	627A	2	0	-5	5	\N
3286	628A	2	0	2	-5	\N
3288	629A	-5	0	1	4	\N
3290	630A	-1	0	5	-3	\N
3292	631D	5	5	2	1	\N
3294	632A	3	0	-6	2	\N
3296	633A	-2	0	-2	-1	\N
3298	634A	1	0	5	-1	\N
3300	635D	4	1	4	-4	\N
3302	636A	-1	0	3	-1	\N
3304	637A	-4	0	2	5	\N
3306	638D	-3	3	2	3	\N
3308	639A	5	0	4	4	\N
3310	640D	-3	1	5	2	\N
3312	641A	-4	0	1	2	\N
3314	642A	5	0	3	5	\N
3316	643D	-1	3	5	0	\N
3318	644C	0	-3	-3	-1	\N
3320	645D	-3	4	-3	-2	\N
3322	646A	-4	0	-3	4	\N
3324	647A	-3	0	-4	-6	\N
3326	648C	-1	2	-4	5	\N
3328	649C	-1	-3	-6	5	\N
3330	650A	-4	0	-6	1	\N
3332	651A	-4	0	-1	0	\N
3334	652C	3	-5	5	-6	\N
3336	653A	2	0	-1	5	\N
3338	654D	1	3	3	2	\N
3340	655C	-4	1	-1	4	\N
3342	656C	-6	-4	-2	1	\N
3344	657D	-2	1	2	-2	\N
3346	658C	4	5	-6	-1	\N
3348	659C	0	-4	-2	0	\N
3350	660D	3	4	-3	1	\N
3352	661C	1	0	-3	1	\N
3354	662A	3	0	-6	-6	\N
3356	663A	4	0	-5	-2	\N
3358	664A	4	0	-5	-1	\N
3360	665A	-3	0	-4	4	\N
3362	666D	1	3	5	4	\N
3364	667A	5	0	0	5	\N
3366	668C	4	-3	3	-3	\N
3368	669A	-4	0	2	1	\N
3370	670D	4	2	4	3	\N
3372	671A	5	0	-4	-5	\N
3374	672A	-5	0	-3	-6	\N
3376	673D	5	2	2	-4	\N
3378	674A	0	0	-6	-1	\N
3380	675A	5	0	3	-2	\N
3382	676A	-2	0	-2	0	\N
3384	677D	1	4	-1	2	\N
3386	678D	-4	1	4	-1	\N
3388	679D	-4	2	-1	3	\N
3390	680D	-6	3	-5	-5	\N
3392	681D	1	5	2	-3	\N
3394	682A	-1	0	1	2	\N
3396	683D	-5	4	3	-5	\N
3398	684C	5	-2	-6	-3	\N
3400	685C	-2	0	5	0	\N
3402	686A	-2	0	4	2	\N
3404	687D	-4	1	0	4	\N
3406	688A	5	0	-4	-6	\N
3408	689A	-6	0	4	-3	\N
3410	690D	1	3	1	-4	\N
3412	691A	5	0	-3	4	\N
3414	692D	-4	1	-2	-3	\N
3416	693D	-2	4	-3	-3	\N
3418	694C	-4	1	-2	4	\N
3420	695C	-1	-1	3	2	\N
3422	696A	2	0	0	1	\N
3424	697A	-6	0	-6	0	\N
3426	698C	5	-6	0	-3	\N
3428	699A	5	0	-1	-3	\N
3430	700A	3	0	-4	0	\N
3432	701D	-3	4	2	4	\N
3434	702C	-4	1	2	3	\N
3436	703A	2	0	0	-6	\N
3438	704A	5	0	3	-4	\N
3440	705A	-6	0	3	0	\N
3442	706A	4	0	3	2	\N
3444	707A	-3	0	4	-6	\N
3446	708C	-6	-3	-3	0	\N
3448	709D	4	2	-2	2	\N
3450	710D	-2	5	-6	5	\N
3452	711C	1	4	-1	-5	\N
3454	712C	4	-6	4	-2	\N
3456	713D	2	1	-1	5	\N
3458	714D	1	2	1	5	\N
3460	715A	-2	0	1	-1	\N
3462	716D	-2	3	-2	0	\N
3464	717C	5	1	1	4	\N
3466	718C	-2	-3	-4	2	\N
3468	719D	3	3	3	-5	\N
3470	720D	-6	1	5	3	\N
3472	721D	-2	2	-4	1	\N
3474	722C	-6	-5	1	5	\N
3476	723A	-5	0	-1	5	\N
3478	724A	2	0	0	3	\N
3480	725A	0	0	-1	1	\N
3482	726D	5	5	3	1	\N
3484	727A	-3	0	1	-4	\N
3486	728A	-2	0	-6	-2	\N
3488	729C	-6	3	-5	4	\N
3490	730A	-1	0	-4	-6	\N
3492	731A	-6	0	0	2	\N
3494	732A	1	0	2	-3	\N
3496	733C	4	4	-6	4	\N
3498	734C	-1	-3	0	1	\N
3500	735A	-2	0	2	1	\N
3502	736D	-2	3	-3	5	\N
3504	737A	-6	0	-3	-2	\N
3506	738D	-4	3	1	-5	\N
3508	739A	-1	0	-4	1	\N
3510	740C	-3	-1	-5	-6	\N
3512	741A	3	0	0	-4	\N
3514	742C	-3	-6	-3	1	\N
3516	743C	4	5	-1	5	\N
3518	744D	0	2	2	2	\N
3520	745A	-1	0	-1	-1	\N
3522	746A	-3	0	0	-1	\N
3524	747A	2	0	4	-2	\N
3526	748D	-4	4	-4	-4	\N
3528	749C	0	2	-1	2	\N
3530	750A	-4	0	-4	2	\N
3532	751A	5	0	2	-1	\N
3534	752A	-4	0	-4	0	\N
3536	753D	3	4	-5	5	\N
3538	754A	-6	0	-5	3	\N
3540	755A	5	0	-6	2	\N
3542	756D	2	2	5	-5	\N
3544	757C	-6	-1	4	0	\N
3546	758A	-6	0	1	-1	\N
3548	759A	-1	0	-2	-4	\N
3550	760D	-4	4	-4	4	\N
3552	761A	2	0	2	-5	\N
3554	762C	-5	-6	5	2	\N
3556	763A	2	0	-4	-4	\N
3558	764A	1	0	4	4	\N
3560	765C	-6	-5	-4	5	\N
3562	766A	1	0	-6	-2	\N
3564	767A	-1	0	3	-2	\N
3566	768A	-6	0	-5	-3	\N
3568	769C	3	-3	4	1	\N
3570	770C	2	-3	2	5	\N
3572	771C	1	4	3	1	\N
3574	772C	-2	1	5	-5	\N
3576	773C	-2	-6	-4	-1	\N
3578	774A	-4	0	-4	-5	\N
3580	775C	2	-1	2	-1	\N
3582	776A	-2	0	3	-4	\N
3584	777C	-6	5	1	5	\N
3586	778A	2	0	2	4	\N
3588	779C	3	-5	-5	-6	\N
3590	780C	-1	-6	-3	-4	\N
3592	781D	-5	5	0	5	\N
3594	782D	-5	4	0	-6	\N
3596	783A	1	0	1	0	\N
3598	784A	2	0	0	5	\N
3600	785A	5	0	-4	-2	\N
3602	786D	-3	3	-4	-2	\N
3604	787A	-3	0	-1	-1	\N
3606	788D	-6	2	3	1	\N
3608	789A	-2	0	-4	-5	\N
3610	790C	-6	5	1	1	\N
3612	791A	3	0	-4	5	\N
3614	792D	-3	5	-5	3	\N
3616	793C	4	-6	-1	-2	\N
3618	794A	2	0	5	3	\N
3620	795C	4	3	2	0	\N
3622	796D	-3	4	4	-3	\N
3624	797A	3	0	-2	5	\N
3626	798D	4	5	2	-4	\N
3628	799A	-2	0	2	-4	\N
3630	800A	5	0	-5	-6	\N
3632	801D	2	5	2	-5	\N
3634	802A	4	0	3	3	\N
3636	803C	2	1	4	-6	\N
3638	804D	-2	3	-1	0	\N
3640	805A	-3	0	0	-3	\N
3642	806A	-6	0	-2	-3	\N
3644	807C	-5	-1	-5	4	\N
3646	808D	1	5	1	1	\N
3648	809A	0	0	-1	0	\N
3650	810A	4	0	0	-5	\N
3652	811A	-5	0	-4	-1	\N
3654	812D	0	1	1	-1	\N
3656	813C	-2	5	2	-1	\N
3658	814A	5	0	-5	-1	\N
3660	815D	-2	4	1	-1	\N
3662	816C	-3	1	-6	5	\N
3664	817D	-6	5	-6	-1	\N
3666	818A	-6	0	-3	-3	\N
3668	819A	-2	0	3	-4	\N
3670	820A	-6	0	1	-1	\N
3672	821C	3	-2	-3	2	\N
3674	822D	2	5	5	-5	\N
3676	823A	-3	0	-3	1	\N
3678	824D	5	2	-3	-4	\N
3680	825D	-1	3	2	-4	\N
3682	826A	0	0	-6	-2	\N
3684	827A	-3	0	0	-1	\N
3686	828A	-1	0	-2	-6	\N
3688	829C	-3	-6	0	-2	\N
3690	830A	3	0	-1	-6	\N
3692	831A	1	0	-6	2	\N
3694	832C	5	-6	3	-1	\N
3696	833A	-3	0	4	2	\N
3698	834A	-6	0	1	1	\N
3700	835A	-1	0	1	5	\N
3702	836A	3	0	2	4	\N
3704	837C	2	4	-1	0	\N
3706	838C	-4	1	3	-5	\N
3708	839C	2	-1	-5	-2	\N
3710	840C	0	0	-3	-4	\N
3712	841C	1	-3	-6	-2	\N
3714	842D	3	1	3	-4	\N
3716	843C	1	3	5	2	\N
3718	844A	-1	0	4	3	\N
3720	845A	-3	0	5	0	\N
3722	846C	-5	-6	4	-1	\N
3724	847C	-3	-6	1	-1	\N
3726	848A	1	0	0	0	\N
3728	849A	-3	0	-2	-5	\N
3730	850A	3	0	-3	2	\N
3732	851D	0	2	-6	-2	\N
3734	852A	2	0	4	-5	\N
3736	853C	2	3	-5	-3	\N
3738	854A	0	0	-4	1	\N
3740	855A	-2	0	1	-5	\N
3742	856A	1	0	2	-6	\N
3744	857A	-3	0	-3	-5	\N
3746	858A	2	0	3	4	\N
3748	859A	2	0	4	5	\N
3750	860C	-4	-3	-4	3	\N
3752	861D	-1	1	-3	-5	\N
3754	862D	-1	2	-3	0	\N
3756	863C	3	-6	-5	0	\N
3758	864A	3	0	3	1	\N
3760	865A	5	0	4	-2	\N
3762	866A	-1	0	-1	0	\N
3764	867D	5	4	4	0	\N
3766	868A	3	0	5	-2	\N
3768	869C	-5	2	1	3	\N
3770	870A	2	0	0	-6	\N
3772	871C	0	-1	-3	-6	\N
3774	872C	-6	-6	2	0	\N
3776	873C	-2	4	-5	-3	\N
3778	874C	-6	-2	-1	2	\N
3780	875A	-2	0	3	5	\N
3782	876D	3	5	3	-5	\N
3784	877C	-1	-3	2	-6	\N
3786	878D	-1	4	2	2	\N
3788	879A	3	0	-1	5	\N
3790	880A	2	0	3	1	\N
3792	881A	5	0	2	-1	\N
3794	882C	4	-6	-2	5	\N
3796	883D	-2	5	1	-5	\N
3798	884A	5	0	-2	4	\N
3800	885A	0	0	-6	-4	\N
3802	886D	-4	3	-2	1	\N
3804	887C	-2	-1	-1	2	\N
3806	888C	-2	-6	5	-1	\N
3808	889A	-2	0	-3	-3	\N
3810	890A	4	0	1	-6	\N
3812	891A	-4	0	1	3	\N
3814	892A	-3	0	-4	3	\N
3816	893C	1	-6	1	1	\N
3818	894C	-1	4	5	3	\N
3820	895A	-3	0	1	5	\N
3822	896A	-4	0	1	4	\N
3824	897C	1	-3	-6	3	\N
3826	898D	-2	4	2	-6	\N
3828	899D	3	5	2	0	\N
3830	900D	-5	2	4	-5	\N
3832	901A	-6	0	-3	-4	\N
3834	902A	-4	0	0	4	\N
3836	903A	1	0	0	3	\N
3838	904A	3	0	4	-6	\N
3840	905C	-4	3	0	-3	\N
3842	906D	1	3	0	5	\N
3844	907D	2	1	-6	0	\N
3846	908C	-5	3	5	2	\N
3848	909A	-6	0	3	3	\N
3850	910A	1	0	-3	4	\N
3852	911A	-5	0	3	1	\N
3854	912C	-3	-5	-3	4	\N
3856	913D	5	4	-3	5	\N
3858	914A	-4	0	-1	-6	\N
3860	915C	1	-4	2	-1	\N
3862	916C	1	0	-1	-4	\N
3864	917A	-1	0	-6	-2	\N
3866	918D	-3	5	5	-3	\N
3868	919A	2	0	-2	2	\N
3870	920A	-3	0	0	3	\N
3872	921A	-1	0	1	1	\N
3874	922C	-5	2	-2	-1	\N
3876	923D	3	1	4	-6	\N
3878	924D	3	5	-1	-1	\N
3880	925A	-4	0	1	-5	\N
3882	926A	0	0	2	-6	\N
3884	927D	2	4	-3	-5	\N
3886	928C	-5	-4	4	-3	\N
3888	929A	-5	0	-5	1	\N
3890	930A	4	0	0	5	\N
3892	931A	-1	0	-3	-1	\N
3894	932C	-4	-4	-1	-2	\N
3896	933A	-4	0	3	-6	\N
3898	934A	2	0	1	3	\N
3900	935C	2	-2	-5	-3	\N
3902	936C	4	3	4	-5	\N
3904	937A	-3	0	-6	2	\N
3906	938C	-3	-6	4	-6	\N
3908	939A	-6	0	1	1	\N
3910	940A	-5	0	-1	-3	\N
3912	941A	-6	0	1	4	\N
3914	942A	2	0	-3	2	\N
3916	943D	-6	3	-4	5	\N
3918	944A	-6	0	-4	2	\N
3920	945A	4	0	3	-4	\N
3922	946A	-4	0	-4	-6	\N
3924	947D	3	3	-3	-1	\N
3926	948A	-4	0	-1	-4	\N
3928	949C	-3	4	-4	-6	\N
3930	950A	5	0	-3	-5	\N
3932	951A	-3	0	-6	-2	\N
3934	952A	-5	0	-2	1	\N
3936	953A	-5	0	-1	0	\N
3938	954C	0	-3	-4	3	\N
3940	955D	-3	3	-5	0	\N
3942	956A	1	0	0	-6	\N
3944	957C	-4	5	4	3	\N
3946	958D	4	5	-5	-1	\N
3948	959D	-2	5	3	1	\N
3950	960C	0	1	-4	0	\N
3952	961D	1	2	4	-3	\N
3954	962A	-6	0	1	3	\N
3956	963D	5	1	5	-2	\N
3958	964A	5	0	-1	-6	\N
3960	965A	2	0	0	3	\N
3962	966A	-2	0	-1	0	\N
3964	967A	1	0	-3	-2	\N
3966	968A	-3	0	-6	2	\N
3968	969A	3	0	-3	-4	\N
3970	970D	-4	5	-4	2	\N
3972	971A	4	0	-3	-2	\N
3974	972D	-6	4	-4	-1	\N
3976	973A	5	0	-1	5	\N
3978	974C	4	-6	-1	4	\N
3980	975A	0	0	-2	5	\N
3982	976D	3	2	-1	-4	\N
3984	977A	5	0	1	-5	\N
3986	978A	-5	0	4	-5	\N
3988	979D	-6	3	0	4	\N
3990	980A	0	0	-2	-6	\N
3992	981C	0	-5	-1	3	\N
3994	982A	2	0	-3	4	\N
3996	983A	-6	0	0	-5	\N
3998	984D	-5	3	-4	-6	\N
4000	985A	2	0	4	2	\N
4002	986D	-4	4	0	-6	\N
4004	987C	-4	-1	1	-6	\N
4006	988C	-5	1	3	-5	\N
4008	989A	-5	0	5	-4	\N
4010	990D	4	5	-3	1	\N
4012	991C	-5	4	4	-2	\N
4014	992A	1	0	-4	2	\N
4016	993A	4	0	1	5	\N
4018	994A	-6	0	-1	-3	\N
4020	995A	-4	0	-5	3	\N
4022	996D	2	1	-2	0	\N
4024	997A	-5	0	-1	3	\N
4026	998D	1	4	4	1	\N
4028	999A	0	0	5	-2	\N
4030	1000C	5	4	5	4	\N
\.


--
-- Data for Name: oggetto_attacco; Type: TABLE DATA; Schema: public; Owner: dungeonasdb
--

COPY oggetto_attacco (id, danno) FROM stdin;
2030	12
2032	7
2034	7
2036	16
2038	4
2040	10
2042	2
2046	16
2048	15
2050	4
2052	8
2054	11
2056	9
2058	16
2060	4
2062	3
2064	6
2066	8
2068	8
2070	14
2076	14
2080	15
2082	9
2094	9
2098	5
2100	4
2102	6
2104	10
2106	8
2112	7
2114	3
2116	11
2118	6
2122	8
2124	14
2126	12
2128	12
2132	12
2138	4
2144	14
2150	6
2152	10
2154	9
2156	14
2158	15
2160	10
2164	10
2172	5
2176	11
2178	5
2180	10
2182	15
2184	12
2186	2
2190	10
2192	11
2194	12
2198	11
2202	10
2210	8
2220	11
2222	8
2224	15
2226	12
2234	9
2238	7
2242	5
2248	4
2254	2
2258	15
2260	12
2268	15
2278	12
2280	10
2282	4
2284	12
2286	5
2296	9
2298	7
2300	3
2302	13
2306	9
2312	16
2316	5
2318	2
2320	16
2324	10
2326	16
2328	16
2332	10
2334	15
2350	11
2356	4
2358	16
2360	14
2364	15
2366	3
2370	16
2372	4
2376	7
2378	7
2382	2
2384	14
2390	2
2396	10
2398	8
2402	11
2404	5
2408	7
2416	16
2420	14
2424	11
2428	6
2430	6
2432	12
2434	13
2446	5
2450	15
2452	3
2454	2
2462	15
2464	13
2466	13
2468	6
2476	9
2478	10
2480	14
2484	14
2488	15
2492	14
2496	14
2498	3
2504	3
2506	8
2510	6
2512	14
2514	5
2518	8
2520	2
2524	4
2534	2
2536	8
2540	9
2546	5
2550	13
2558	11
2560	15
2562	3
2572	10
2576	14
2586	15
2588	2
2590	12
2594	10
2596	14
2606	9
2608	3
2614	16
2616	8
2626	15
2632	13
2634	10
2644	14
2646	6
2648	2
2656	5
2660	10
2662	8
2666	3
2668	8
2672	2
2680	9
2682	13
2684	16
2694	6
2696	14
2698	16
2700	16
2708	14
2710	9
2712	15
2718	5
2720	6
2722	5
2728	13
2732	14
2740	15
2744	15
2750	14
2752	9
2754	10
2758	13
2762	4
2772	9
2774	14
2776	6
2782	9
2786	15
2788	14
2790	12
2796	15
2800	10
2806	11
2808	5
2810	11
2812	12
2816	9
2818	12
2822	10
2828	16
2830	10
2832	2
2838	14
2840	7
2842	14
2844	15
2848	8
2852	16
2868	15
2870	13
2874	15
2880	11
2886	11
2894	7
2908	4
2910	10
2914	11
2916	5
2920	2
2924	6
2934	14
2940	5
2942	2
2944	15
2946	16
2948	10
2950	6
2956	2
2958	2
2964	3
2968	15
2970	10
2972	11
2976	10
2978	15
2980	16
2986	4
2988	16
2994	5
3000	12
3006	6
3014	10
3022	16
3026	13
3038	8
3042	14
3044	6
3046	14
3048	9
3052	13
3054	11
3056	3
3060	4
3062	2
3064	6
3070	14
3072	2
3076	16
3078	16
3086	11
3090	11
3092	8
3094	16
3096	6
3098	13
3104	11
3108	2
3112	3
3116	11
3118	2
3120	11
3124	2
3126	7
3132	13
3136	5
3138	13
3140	5
3148	4
3150	2
3152	9
3154	5
3156	2
3158	15
3164	7
3174	2
3178	9
3180	11
3182	4
3186	7
3200	11
3208	5
3210	7
3214	3
3216	7
3220	10
3224	11
3228	12
3230	8
3234	2
3236	14
3238	14
3240	3
3242	4
3244	13
3246	13
3252	4
3256	16
3260	5
3262	2
3264	10
3268	14
3270	15
3272	11
3274	9
3280	12
3282	5
3284	11
3286	2
3288	16
3290	9
3294	11
3296	7
3298	15
3302	12
3304	7
3308	14
3312	14
3314	3
3322	5
3324	12
3330	8
3332	7
3336	9
3354	3
3356	13
3358	16
3360	8
3364	11
3368	12
3372	13
3374	16
3378	16
3380	4
3382	4
3394	10
3402	14
3406	14
3408	11
3412	8
3422	14
3424	5
3428	12
3430	6
3436	7
3438	10
3440	3
3442	5
3444	2
3460	5
3476	9
3478	11
3480	10
3484	10
3486	10
3490	13
3492	10
3494	6
3500	10
3504	10
3508	10
3512	2
3520	2
3522	10
3524	13
3530	5
3532	6
3534	15
3538	14
3540	5
3546	5
3548	4
3552	3
3556	9
3558	2
3562	12
3564	5
3566	13
3578	8
3582	16
3586	16
3596	8
3598	2
3600	6
3604	4
3608	4
3612	11
3618	4
3624	5
3628	10
3630	8
3634	11
3640	6
3642	6
3648	16
3650	14
3652	9
3658	3
3666	6
3668	3
3670	5
3676	10
3682	6
3684	14
3686	7
3690	15
3692	14
3696	11
3698	4
3700	8
3702	8
3718	11
3720	14
3726	12
3728	2
3730	2
3734	10
3738	6
3740	11
3742	16
3744	16
3746	5
3748	7
3758	2
3760	6
3762	9
3766	13
3770	15
3780	7
3788	15
3790	10
3792	12
3798	6
3800	11
3808	10
3810	6
3812	13
3814	10
3820	7
3822	4
3832	10
3834	5
3836	6
3838	13
3848	16
3850	15
3852	6
3858	8
3864	10
3868	14
3870	4
3872	6
3880	2
3882	14
3888	15
3890	10
3892	3
3896	16
3898	9
3904	7
3908	3
3910	14
3912	14
3914	4
3918	6
3920	10
3922	2
3926	2
3930	15
3932	2
3934	14
3936	6
3942	10
3954	14
3958	16
3960	3
3962	11
3964	3
3966	7
3968	4
3972	15
3976	5
3980	4
3984	7
3986	3
3990	5
3994	12
3996	11
4000	3
4008	8
4014	14
4016	9
4018	15
4020	9
4024	6
4028	4
\.


--
-- Data for Name: oggetto_consumabile; Type: TABLE DATA; Schema: public; Owner: dungeonasdb
--

COPY oggetto_consumabile (id) FROM stdin;
2044
2072
2078
2084
2086
2092
2108
2120
2130
2134
2136
2142
2146
2162
2166
2170
2174
2204
2206
2212
2214
2216
2228
2240
2250
2252
2262
2308
2330
2336
2340
2342
2344
2362
2374
2380
2412
2438
2442
2444
2448
2458
2470
2472
2474
2490
2494
2502
2508
2522
2528
2530
2532
2538
2548
2552
2554
2566
2574
2578
2580
2582
2584
2592
2602
2612
2620
2622
2628
2636
2642
2650
2652
2654
2658
2678
2692
2702
2704
2724
2726
2738
2742
2748
2756
2778
2780
2784
2792
2820
2826
2836
2846
2850
2856
2858
2864
2866
2872
2876
2878
2884
2888
2900
2904
2912
2918
2922
2930
2932
2954
2960
2962
2974
2982
2984
2996
3004
3010
3016
3018
3020
3024
3030
3032
3034
3040
3058
3068
3082
3084
3088
3102
3106
3122
3128
3142
3144
3146
3166
3170
3172
3176
3184
3188
3190
3196
3202
3204
3206
3212
3222
3248
3250
3258
3266
3278
3318
3326
3328
3334
3340
3342
3346
3348
3352
3366
3398
3400
3418
3420
3426
3434
3446
3452
3454
3464
3466
3474
3488
3496
3498
3510
3514
3516
3528
3544
3554
3560
3568
3570
3572
3574
3576
3580
3584
3588
3590
3610
3616
3620
3636
3644
3656
3662
3672
3688
3694
3704
3706
3708
3710
3712
3716
3722
3724
3736
3750
3756
3768
3772
3774
3776
3778
3784
3794
3804
3806
3816
3818
3824
3840
3846
3854
3860
3862
3874
3886
3894
3900
3902
3906
3928
3938
3944
3950
3978
3992
4004
4006
4012
4030
\.


--
-- Name: oggetto_consumabile_utilizzato_in_stanza_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dungeonasdb
--

SELECT pg_catalog.setval('oggetto_consumabile_utilizzato_in_stanza_id_seq', 4, true);


--
-- Data for Name: oggetto_difesa; Type: TABLE DATA; Schema: public; Owner: dungeonasdb
--

COPY oggetto_difesa (id) FROM stdin;
2074
2088
2090
2096
2110
2140
2148
2168
2188
2196
2200
2208
2218
2230
2232
2236
2244
2246
2256
2264
2266
2270
2272
2274
2276
2288
2290
2292
2294
2304
2310
2314
2322
2338
2346
2348
2352
2354
2368
2386
2388
2392
2394
2400
2406
2410
2414
2418
2422
2426
2436
2440
2456
2460
2482
2486
2500
2516
2526
2542
2544
2556
2564
2568
2570
2598
2600
2604
2610
2618
2624
2630
2638
2640
2664
2670
2674
2676
2686
2688
2690
2706
2714
2716
2730
2734
2736
2746
2760
2764
2766
2768
2770
2794
2798
2802
2804
2814
2824
2834
2854
2860
2862
2882
2890
2892
2896
2898
2902
2906
2926
2928
2936
2938
2952
2966
2990
2992
2998
3002
3008
3012
3028
3036
3050
3066
3074
3080
3100
3110
3114
3130
3134
3160
3162
3168
3192
3194
3198
3218
3226
3232
3254
3276
3292
3300
3306
3310
3316
3320
3338
3344
3350
3362
3370
3376
3384
3386
3388
3390
3392
3396
3404
3410
3414
3416
3432
3448
3450
3456
3458
3462
3468
3470
3472
3482
3502
3506
3518
3526
3536
3542
3550
3592
3594
3602
3606
3614
3622
3626
3632
3638
3646
3654
3660
3664
3674
3678
3680
3714
3732
3752
3754
3764
3782
3786
3796
3802
3826
3828
3830
3842
3844
3856
3866
3876
3878
3884
3916
3924
3940
3946
3948
3952
3956
3970
3974
3982
3988
3998
4002
4010
4022
4026
\.


--
-- Name: oggetto_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dungeonasdb
--

SELECT pg_catalog.setval('oggetto_id_seq', 4030, true);


--
-- Data for Name: oggetto_utilizzato_in_stanza; Type: TABLE DATA; Schema: public; Owner: dungeonasdb
--

COPY oggetto_utilizzato_in_stanza (id, oggetto, stanza, dungeon_giocato) FROM stdin;
\.


--
-- Data for Name: passaggio; Type: TABLE DATA; Schema: public; Owner: dungeonasdb
--

COPY passaggio (stanza_provenienza, stanza_arrivo, dungeon, id, segreto) FROM stdin;
\N	36	81	3109	f
33	\N	81	3110	f
36	28	81	3111	f
28	36	81	3112	f
28	14	81	3113	f
14	28	81	3114	f
14	33	81	3115	f
33	14	81	3116	f
\.


--
-- Name: passaggio_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dungeonasdb
--

SELECT pg_catalog.setval('passaggio_id_seq', 3116, true);


--
-- Data for Name: personaggio; Type: TABLE DATA; Schema: public; Owner: dungeonasdb
--

COPY personaggio (id, nome, descrizione, forza, intelligenza, costituzione, agilita, esperienza, monete_oro, punti_ferita, utente, punti_ferita_iniziali) FROM stdin;
2	bb	bb	12	12	12	12	0	0	12	paolo@astag.it	\N
1	Pablo	Aggressivo e perspicace	3	3	3	3	0	0	100	paolo@astag.it	\N
\.


--
-- Name: personaggio_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dungeonasdb
--

SELECT pg_catalog.setval('personaggio_id_seq', 2, true);


--
-- Data for Name: posizione_oggetto; Type: TABLE DATA; Schema: public; Owner: dungeonasdb
--

COPY posizione_oggetto (oggetto, visibile, stanza, dungeon, id) FROM stdin;
3448	t	36	81	146738
2330	f	36	81	146739
2234	t	36	81	146740
3648	t	36	81	146741
2370	t	36	81	146742
2636	f	28	81	146743
2528	f	28	81	146744
2770	t	28	81	146745
2626	t	28	81	146746
3984	t	28	81	146747
2216	f	28	81	146748
3982	t	28	81	146749
2726	f	28	81	146750
2968	t	28	81	146751
2262	t	28	81	146752
2654	f	14	81	146753
3464	f	14	81	146754
2326	t	14	81	146755
3012	f	14	81	146756
2474	f	14	81	146757
3766	f	14	81	146758
3090	f	14	81	146759
3334	t	14	81	146760
3038	f	14	81	146761
2762	f	14	81	146762
2994	t	14	81	146763
3256	t	14	81	146764
2038	t	14	81	146765
2990	f	14	81	146766
2498	t	14	81	146767
2832	f	33	81	146768
2876	f	33	81	146769
2046	t	33	81	146770
3908	f	33	81	146771
3090	f	33	81	146772
2352	t	33	81	146773
3406	t	33	81	146774
3062	f	33	81	146775
3216	t	33	81	146776
2614	t	33	81	146777
3036	f	33	81	146778
2872	f	33	81	146779
2496	t	33	81	146780
2186	t	33	81	146781
3828	f	33	81	146782
2652	t	33	81	146783
3746	t	33	81	146784
2870	f	33	81	146785
2646	f	33	81	146786
2764	t	33	81	146787
\.


--
-- Name: posizione_oggetto_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dungeonasdb
--

SELECT pg_catalog.setval('posizione_oggetto_id_seq', 146787, true);


--
-- Data for Name: spostamenti_personaggio; Type: TABLE DATA; Schema: public; Owner: dungeonasdb
--

COPY spostamenti_personaggio (id, stanza, attualmente, dungeon_giocato) FROM stdin;
\.


--
-- Name: spostamenti_personaggio_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dungeonasdb
--

SELECT pg_catalog.setval('spostamenti_personaggio_id_seq', 3, true);


--
-- Data for Name: stanza; Type: TABLE DATA; Schema: public; Owner: dungeonasdb
--

COPY stanza (id, descrizione) FROM stdin;
1	Stanza Terrificante
2	Stanza del fuoco
5	
6	
7	
8	
9	
10	
11	
12	
13	
14	
15	
16	
17	
18	
19	
20	
21	
22	
23	
24	
25	
26	
27	
28	
29	
30	
31	
32	
33	
34	
35	
36	
37	
38	ABA
3	Stanza di Guido
4	Stanza di Marco
39	Stanza di Ghiaccio
\.


--
-- Name: stanza_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dungeonasdb
--

SELECT pg_catalog.setval('stanza_id_seq', 39, true);


--
-- Data for Name: transazione; Type: TABLE DATA; Schema: public; Owner: dungeonasdb
--

COPY transazione (id_cedente, id_acquirente, oggetto_cedente, oggetto_acquirente, data_transazione, finita, id, scambio) FROM stdin;
\.


--
-- Name: transazione_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dungeonasdb
--

SELECT pg_catalog.setval('transazione_id_seq', 3, true);


--
-- Data for Name: utente; Type: TABLE DATA; Schema: public; Owner: dungeonasdb
--

COPY utente (email, password, id) FROM stdin;
paolo@astag.it	Milan88	Paulinho
\.


--
-- Name: attacco attacco_pkey; Type: CONSTRAINT; Schema: public; Owner: dungeonasdb
--

ALTER TABLE ONLY attacco
    ADD CONSTRAINT attacco_pkey PRIMARY KEY (id);


--
-- Name: dungeon_giocato dungeon_giocato_pkey; Type: CONSTRAINT; Schema: public; Owner: dungeonasdb
--

ALTER TABLE ONLY dungeon_giocato
    ADD CONSTRAINT dungeon_giocato_pkey PRIMARY KEY (id);


--
-- Name: dungeon dungeon_pkey; Type: CONSTRAINT; Schema: public; Owner: dungeonasdb
--

ALTER TABLE ONLY dungeon
    ADD CONSTRAINT dungeon_pkey PRIMARY KEY (id);


--
-- Name: nemici_in_stanza nemici_in_stanza_pkey; Type: CONSTRAINT; Schema: public; Owner: dungeonasdb
--

ALTER TABLE ONLY nemici_in_stanza
    ADD CONSTRAINT nemici_in_stanza_pkey PRIMARY KEY (nemico, stanza, dungeon);


--
-- Name: nemici_sconfitti nemici_sconfitti_pkey; Type: CONSTRAINT; Schema: public; Owner: dungeonasdb
--

ALTER TABLE ONLY nemici_sconfitti
    ADD CONSTRAINT nemici_sconfitti_pkey PRIMARY KEY (id);


--
-- Name: nemico nemico_pkey; Type: CONSTRAINT; Schema: public; Owner: dungeonasdb
--

ALTER TABLE ONLY nemico
    ADD CONSTRAINT nemico_pkey PRIMARY KEY (id);


--
-- Name: oggetti_posseduti oggetti_posseduti_pkey; Type: CONSTRAINT; Schema: public; Owner: dungeonasdb
--

ALTER TABLE ONLY oggetti_posseduti
    ADD CONSTRAINT oggetti_posseduti_pkey PRIMARY KEY (personaggio, oggetto);


--
-- Name: oggetto_attacco oggetto_attacco_pkey; Type: CONSTRAINT; Schema: public; Owner: dungeonasdb
--

ALTER TABLE ONLY oggetto_attacco
    ADD CONSTRAINT oggetto_attacco_pkey PRIMARY KEY (id);


--
-- Name: oggetto_consumabile oggetto_consumabile_difesa_pkey; Type: CONSTRAINT; Schema: public; Owner: dungeonasdb
--

ALTER TABLE ONLY oggetto_consumabile
    ADD CONSTRAINT oggetto_consumabile_difesa_pkey PRIMARY KEY (id);


--
-- Name: oggetto_utilizzato_in_stanza oggetto_consumabile_utilizzato_in_stanza_pkey; Type: CONSTRAINT; Schema: public; Owner: dungeonasdb
--

ALTER TABLE ONLY oggetto_utilizzato_in_stanza
    ADD CONSTRAINT oggetto_consumabile_utilizzato_in_stanza_pkey PRIMARY KEY (id);


--
-- Name: oggetto_difesa oggetto_difesa_pkey; Type: CONSTRAINT; Schema: public; Owner: dungeonasdb
--

ALTER TABLE ONLY oggetto_difesa
    ADD CONSTRAINT oggetto_difesa_pkey PRIMARY KEY (id);


--
-- Name: oggetto oggetto_nome_key; Type: CONSTRAINT; Schema: public; Owner: dungeonasdb
--

ALTER TABLE ONLY oggetto
    ADD CONSTRAINT oggetto_nome_key UNIQUE (nome);


--
-- Name: oggetto oggetto_pkey; Type: CONSTRAINT; Schema: public; Owner: dungeonasdb
--

ALTER TABLE ONLY oggetto
    ADD CONSTRAINT oggetto_pkey PRIMARY KEY (id);


--
-- Name: personaggio personaggio_nome_key; Type: CONSTRAINT; Schema: public; Owner: dungeonasdb
--

ALTER TABLE ONLY personaggio
    ADD CONSTRAINT personaggio_nome_key UNIQUE (nome);


--
-- Name: personaggio personaggio_pkey; Type: CONSTRAINT; Schema: public; Owner: dungeonasdb
--

ALTER TABLE ONLY personaggio
    ADD CONSTRAINT personaggio_pkey PRIMARY KEY (id);


--
-- Name: posizione_oggetto posizione_oggetto_pkey; Type: CONSTRAINT; Schema: public; Owner: dungeonasdb
--

ALTER TABLE ONLY posizione_oggetto
    ADD CONSTRAINT posizione_oggetto_pkey PRIMARY KEY (id);


--
-- Name: spostamenti_personaggio spostamenti_personaggio_pkey; Type: CONSTRAINT; Schema: public; Owner: dungeonasdb
--

ALTER TABLE ONLY spostamenti_personaggio
    ADD CONSTRAINT spostamenti_personaggio_pkey PRIMARY KEY (id);


--
-- Name: stanza stanza_pkey; Type: CONSTRAINT; Schema: public; Owner: dungeonasdb
--

ALTER TABLE ONLY stanza
    ADD CONSTRAINT stanza_pkey PRIMARY KEY (id);


--
-- Name: transazione transazione_pkey; Type: CONSTRAINT; Schema: public; Owner: dungeonasdb
--

ALTER TABLE ONLY transazione
    ADD CONSTRAINT transazione_pkey PRIMARY KEY (id);


--
-- Name: utente utente_id_key; Type: CONSTRAINT; Schema: public; Owner: dungeonasdb
--

ALTER TABLE ONLY utente
    ADD CONSTRAINT utente_id_key UNIQUE (id);


--
-- Name: utente utente_pkey; Type: CONSTRAINT; Schema: public; Owner: dungeonasdb
--

ALTER TABLE ONLY utente
    ADD CONSTRAINT utente_pkey PRIMARY KEY (email);


--
-- Name: attacco attacco_dungeon_fkey; Type: FK CONSTRAINT; Schema: public; Owner: dungeonasdb
--

ALTER TABLE ONLY attacco
    ADD CONSTRAINT attacco_dungeon_fkey FOREIGN KEY (dungeon) REFERENCES dungeon(id);


--
-- Name: attacco attacco_nemico_fkey; Type: FK CONSTRAINT; Schema: public; Owner: dungeonasdb
--

ALTER TABLE ONLY attacco
    ADD CONSTRAINT attacco_nemico_fkey FOREIGN KEY (nemico) REFERENCES nemico(id);


--
-- Name: attacco attacco_personaggio_fkey; Type: FK CONSTRAINT; Schema: public; Owner: dungeonasdb
--

ALTER TABLE ONLY attacco
    ADD CONSTRAINT attacco_personaggio_fkey FOREIGN KEY (personaggio) REFERENCES personaggio(id);


--
-- Name: attacco attacco_stanza_fkey; Type: FK CONSTRAINT; Schema: public; Owner: dungeonasdb
--

ALTER TABLE ONLY attacco
    ADD CONSTRAINT attacco_stanza_fkey FOREIGN KEY (stanza) REFERENCES stanza(id);


--
-- Name: dungeon_giocato dungeon_superati_dungeon_fkey; Type: FK CONSTRAINT; Schema: public; Owner: dungeonasdb
--

ALTER TABLE ONLY dungeon_giocato
    ADD CONSTRAINT dungeon_superati_dungeon_fkey FOREIGN KEY (dungeon) REFERENCES dungeon(id);


--
-- Name: nemici_in_stanza nemici_in_stanza_dungeon_fkey; Type: FK CONSTRAINT; Schema: public; Owner: dungeonasdb
--

ALTER TABLE ONLY nemici_in_stanza
    ADD CONSTRAINT nemici_in_stanza_dungeon_fkey FOREIGN KEY (dungeon) REFERENCES dungeon(id);


--
-- Name: nemici_in_stanza nemici_in_stanza_nemico_fkey; Type: FK CONSTRAINT; Schema: public; Owner: dungeonasdb
--

ALTER TABLE ONLY nemici_in_stanza
    ADD CONSTRAINT nemici_in_stanza_nemico_fkey FOREIGN KEY (nemico) REFERENCES nemico(id);


--
-- Name: nemici_in_stanza nemici_in_stanza_stanza_fkey; Type: FK CONSTRAINT; Schema: public; Owner: dungeonasdb
--

ALTER TABLE ONLY nemici_in_stanza
    ADD CONSTRAINT nemici_in_stanza_stanza_fkey FOREIGN KEY (stanza) REFERENCES stanza(id);


--
-- Name: nemici_sconfitti nemici_sconfitti_dungeon_giocato_fkey; Type: FK CONSTRAINT; Schema: public; Owner: dungeonasdb
--

ALTER TABLE ONLY nemici_sconfitti
    ADD CONSTRAINT nemici_sconfitti_dungeon_giocato_fkey FOREIGN KEY (dungeon_giocato) REFERENCES dungeon_giocato(id);


--
-- Name: nemici_sconfitti nemici_sconfitti_nemico_fkey; Type: FK CONSTRAINT; Schema: public; Owner: dungeonasdb
--

ALTER TABLE ONLY nemici_sconfitti
    ADD CONSTRAINT nemici_sconfitti_nemico_fkey FOREIGN KEY (nemico) REFERENCES nemico(id);


--
-- Name: oggetti_posseduti oggetti_posseduti_oggetto_fkey; Type: FK CONSTRAINT; Schema: public; Owner: dungeonasdb
--

ALTER TABLE ONLY oggetti_posseduti
    ADD CONSTRAINT oggetti_posseduti_oggetto_fkey FOREIGN KEY (oggetto) REFERENCES oggetto(id);


--
-- Name: oggetti_posseduti oggetti_posseduti_personaggio_fkey; Type: FK CONSTRAINT; Schema: public; Owner: dungeonasdb
--

ALTER TABLE ONLY oggetti_posseduti
    ADD CONSTRAINT oggetti_posseduti_personaggio_fkey FOREIGN KEY (personaggio) REFERENCES personaggio(id);


--
-- Name: oggetto_attacco oggetto_attacco_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: dungeonasdb
--

ALTER TABLE ONLY oggetto_attacco
    ADD CONSTRAINT oggetto_attacco_id_fkey FOREIGN KEY (id) REFERENCES oggetto(id);


--
-- Name: oggetto_consumabile oggetto_consumabile_difesa_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: dungeonasdb
--

ALTER TABLE ONLY oggetto_consumabile
    ADD CONSTRAINT oggetto_consumabile_difesa_id_fkey FOREIGN KEY (id) REFERENCES oggetto(id);


--
-- Name: oggetto_utilizzato_in_stanza oggetto_consumabile_utilizzato_in_stanza_oggetto_fkey; Type: FK CONSTRAINT; Schema: public; Owner: dungeonasdb
--

ALTER TABLE ONLY oggetto_utilizzato_in_stanza
    ADD CONSTRAINT oggetto_consumabile_utilizzato_in_stanza_oggetto_fkey FOREIGN KEY (oggetto) REFERENCES oggetto_consumabile(id);


--
-- Name: oggetto_utilizzato_in_stanza oggetto_consumabile_utilizzato_in_stanza_stanza_fkey; Type: FK CONSTRAINT; Schema: public; Owner: dungeonasdb
--

ALTER TABLE ONLY oggetto_utilizzato_in_stanza
    ADD CONSTRAINT oggetto_consumabile_utilizzato_in_stanza_stanza_fkey FOREIGN KEY (stanza) REFERENCES stanza(id);


--
-- Name: oggetto_difesa oggetto_difesa_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: dungeonasdb
--

ALTER TABLE ONLY oggetto_difesa
    ADD CONSTRAINT oggetto_difesa_id_fkey FOREIGN KEY (id) REFERENCES oggetto(id);


--
-- Name: oggetto_utilizzato_in_stanza oggetto_utilizzato_in_stanza_dungeon_giocato_fkey; Type: FK CONSTRAINT; Schema: public; Owner: dungeonasdb
--

ALTER TABLE ONLY oggetto_utilizzato_in_stanza
    ADD CONSTRAINT oggetto_utilizzato_in_stanza_dungeon_giocato_fkey FOREIGN KEY (dungeon_giocato) REFERENCES dungeon_giocato(id);


--
-- Name: passaggio passaggio_dungeon_fkey; Type: FK CONSTRAINT; Schema: public; Owner: dungeonasdb
--

ALTER TABLE ONLY passaggio
    ADD CONSTRAINT passaggio_dungeon_fkey FOREIGN KEY (dungeon) REFERENCES dungeon(id);


--
-- Name: passaggio passaggio_stanza_arrivo_fkey; Type: FK CONSTRAINT; Schema: public; Owner: dungeonasdb
--

ALTER TABLE ONLY passaggio
    ADD CONSTRAINT passaggio_stanza_arrivo_fkey FOREIGN KEY (stanza_arrivo) REFERENCES stanza(id);


--
-- Name: passaggio passaggio_stanza_provenienza_fkey; Type: FK CONSTRAINT; Schema: public; Owner: dungeonasdb
--

ALTER TABLE ONLY passaggio
    ADD CONSTRAINT passaggio_stanza_provenienza_fkey FOREIGN KEY (stanza_provenienza) REFERENCES stanza(id);


--
-- Name: personaggio personaggio_utente_fkey; Type: FK CONSTRAINT; Schema: public; Owner: dungeonasdb
--

ALTER TABLE ONLY personaggio
    ADD CONSTRAINT personaggio_utente_fkey FOREIGN KEY (utente) REFERENCES utente(email);


--
-- Name: posizione_oggetto posizione_oggetto_dungeon_fkey; Type: FK CONSTRAINT; Schema: public; Owner: dungeonasdb
--

ALTER TABLE ONLY posizione_oggetto
    ADD CONSTRAINT posizione_oggetto_dungeon_fkey FOREIGN KEY (dungeon) REFERENCES dungeon(id);


--
-- Name: posizione_oggetto posizione_oggetto_oggetto_fkey; Type: FK CONSTRAINT; Schema: public; Owner: dungeonasdb
--

ALTER TABLE ONLY posizione_oggetto
    ADD CONSTRAINT posizione_oggetto_oggetto_fkey FOREIGN KEY (oggetto) REFERENCES oggetto(id);


--
-- Name: posizione_oggetto posizione_oggetto_stanza_fkey; Type: FK CONSTRAINT; Schema: public; Owner: dungeonasdb
--

ALTER TABLE ONLY posizione_oggetto
    ADD CONSTRAINT posizione_oggetto_stanza_fkey FOREIGN KEY (stanza) REFERENCES stanza(id);


--
-- Name: spostamenti_personaggio spostamenti_personaggio_dungeon_giocato_fkey; Type: FK CONSTRAINT; Schema: public; Owner: dungeonasdb
--

ALTER TABLE ONLY spostamenti_personaggio
    ADD CONSTRAINT spostamenti_personaggio_dungeon_giocato_fkey FOREIGN KEY (dungeon_giocato) REFERENCES dungeon_giocato(id);


--
-- Name: spostamenti_personaggio spostamenti_personaggio_stanza_fkey; Type: FK CONSTRAINT; Schema: public; Owner: dungeonasdb
--

ALTER TABLE ONLY spostamenti_personaggio
    ADD CONSTRAINT spostamenti_personaggio_stanza_fkey FOREIGN KEY (stanza) REFERENCES stanza(id);


--
-- Name: transazione transazione_id_acquirente_fkey; Type: FK CONSTRAINT; Schema: public; Owner: dungeonasdb
--

ALTER TABLE ONLY transazione
    ADD CONSTRAINT transazione_id_acquirente_fkey FOREIGN KEY (id_acquirente) REFERENCES personaggio(id);


--
-- Name: transazione transazione_id_cedente_fkey; Type: FK CONSTRAINT; Schema: public; Owner: dungeonasdb
--

ALTER TABLE ONLY transazione
    ADD CONSTRAINT transazione_id_cedente_fkey FOREIGN KEY (id_cedente) REFERENCES personaggio(id);


--
-- Name: transazione transazione_oggetto_acquirente_fkey; Type: FK CONSTRAINT; Schema: public; Owner: dungeonasdb
--

ALTER TABLE ONLY transazione
    ADD CONSTRAINT transazione_oggetto_acquirente_fkey FOREIGN KEY (oggetto_acquirente) REFERENCES oggetto(id);


--
-- Name: transazione transazione_oggetto_cedente_fkey; Type: FK CONSTRAINT; Schema: public; Owner: dungeonasdb
--

ALTER TABLE ONLY transazione
    ADD CONSTRAINT transazione_oggetto_cedente_fkey FOREIGN KEY (oggetto_cedente) REFERENCES oggetto(id);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO dungeonasdb;


--
-- PostgreSQL database dump complete
--

